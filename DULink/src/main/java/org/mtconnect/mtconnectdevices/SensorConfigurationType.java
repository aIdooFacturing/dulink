//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:02 AM KST 
//


package org.mtconnect.mtconnectdevices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *         Calibration data for a sensor
 *       
 * 
 * <p>SensorConfigurationType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="SensorConfigurationType">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:mtconnect.org:MTConnectDevices:1.3}AbstractConfigurationType">
 *       &lt;sequence>
 *         &lt;element name="FirmwareVersion" type="{urn:mtconnect.org:MTConnectDevices:1.3}FirmwareVersionType"/>
 *         &lt;element name="CalibrationDate" type="{urn:mtconnect.org:MTConnectDevices:1.3}CalibrationDateType" minOccurs="0"/>
 *         &lt;element name="NextCalibrationDate" type="{urn:mtconnect.org:MTConnectDevices:1.3}NextCalibrationDateType" minOccurs="0"/>
 *         &lt;element name="CalibrationInitials" type="{urn:mtconnect.org:MTConnectDevices:1.3}CalibrationInitialsType" minOccurs="0"/>
 *         &lt;element name="Channels" type="{urn:mtconnect.org:MTConnectDevices:1.3}ChannelsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SensorConfigurationType", propOrder = {
    "firmwareVersion",
    "calibrationDate",
    "nextCalibrationDate",
    "calibrationInitials",
    "channels"
})
public class SensorConfigurationType
    extends AbstractConfigurationType
{

    @XmlElement(name = "FirmwareVersion", required = true)
    protected String firmwareVersion;
    @XmlElement(name = "CalibrationDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar calibrationDate;
    @XmlElement(name = "NextCalibrationDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nextCalibrationDate;
    @XmlElement(name = "CalibrationInitials")
    protected String calibrationInitials;
    @XmlElement(name = "Channels")
    protected ChannelsType channels;

    /**
     * firmwareVersion 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    /**
     * firmwareVersion 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirmwareVersion(String value) {
        this.firmwareVersion = value;
    }

    /**
     * calibrationDate 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCalibrationDate() {
        return calibrationDate;
    }

    /**
     * calibrationDate 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCalibrationDate(XMLGregorianCalendar value) {
        this.calibrationDate = value;
    }

    /**
     * nextCalibrationDate 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNextCalibrationDate() {
        return nextCalibrationDate;
    }

    /**
     * nextCalibrationDate 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNextCalibrationDate(XMLGregorianCalendar value) {
        this.nextCalibrationDate = value;
    }

    /**
     * calibrationInitials 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalibrationInitials() {
        return calibrationInitials;
    }

    /**
     * calibrationInitials 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalibrationInitials(String value) {
        this.calibrationInitials = value;
    }

    /**
     * channels 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ChannelsType }
     *     
     */
    public ChannelsType getChannels() {
        return channels;
    }

    /**
     * channels 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelsType }
     *     
     */
    public void setChannels(ChannelsType value) {
        this.channels = value;
    }

}
