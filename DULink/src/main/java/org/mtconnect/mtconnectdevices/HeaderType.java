//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:02 AM KST 
//


package org.mtconnect.mtconnectdevices;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * 
 *         Message header for protocol information
 *       
 * 
 * <p>HeaderType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="HeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssetCounts" type="{urn:mtconnect.org:MTConnectDevices:1.3}AssetCountsType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="version" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}VersionType" />
 *       &lt;attribute name="creationTime" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}CreationTimeType" />
 *       &lt;attribute name="testIndicator" type="{urn:mtconnect.org:MTConnectDevices:1.3}TestIndicatorType" />
 *       &lt;attribute name="instanceId" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}InstanceIdType" />
 *       &lt;attribute name="sender" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}SenderType" />
 *       &lt;attribute name="bufferSize" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}BufferSizeType" />
 *       &lt;attribute name="assetBufferSize" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}AssetBufferSizeType" />
 *       &lt;attribute name="assetCount" use="required" type="{urn:mtconnect.org:MTConnectDevices:1.3}AssetCountAttrType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderType", propOrder = {
    "assetCounts"
})
public class HeaderType {

    @XmlElement(name = "AssetCounts")
    protected AssetCountsType assetCounts;
    @XmlAttribute(name = "version", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String version;
    @XmlAttribute(name = "creationTime", required = true)
    protected XMLGregorianCalendar creationTime;
    @XmlAttribute(name = "testIndicator")
    protected Boolean testIndicator;
    @XmlAttribute(name = "instanceId", required = true)
    protected BigInteger instanceId;
    @XmlAttribute(name = "sender", required = true)
    protected String sender;
    @XmlAttribute(name = "bufferSize", required = true)
    protected long bufferSize;
    @XmlAttribute(name = "assetBufferSize", required = true)
    protected long assetBufferSize;
    @XmlAttribute(name = "assetCount", required = true)
    protected long assetCount;

    /**
     * assetCounts 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link AssetCountsType }
     *     
     */
    public AssetCountsType getAssetCounts() {
        return assetCounts;
    }

    /**
     * assetCounts 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link AssetCountsType }
     *     
     */
    public void setAssetCounts(AssetCountsType value) {
        this.assetCounts = value;
    }

    /**
     * version 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * version 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * creationTime 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationTime() {
        return creationTime;
    }

    /**
     * creationTime 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationTime(XMLGregorianCalendar value) {
        this.creationTime = value;
    }

    /**
     * testIndicator 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTestIndicator() {
        return testIndicator;
    }

    /**
     * testIndicator 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTestIndicator(Boolean value) {
        this.testIndicator = value;
    }

    /**
     * instanceId 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInstanceId() {
        return instanceId;
    }

    /**
     * instanceId 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInstanceId(BigInteger value) {
        this.instanceId = value;
    }

    /**
     * sender 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSender() {
        return sender;
    }

    /**
     * sender 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSender(String value) {
        this.sender = value;
    }

    /**
     * bufferSize 속성의 값을 가져옵니다.
     * 
     */
    public long getBufferSize() {
        return bufferSize;
    }

    /**
     * bufferSize 속성의 값을 설정합니다.
     * 
     */
    public void setBufferSize(long value) {
        this.bufferSize = value;
    }

    /**
     * assetBufferSize 속성의 값을 가져옵니다.
     * 
     */
    public long getAssetBufferSize() {
        return assetBufferSize;
    }

    /**
     * assetBufferSize 속성의 값을 설정합니다.
     * 
     */
    public void setAssetBufferSize(long value) {
        this.assetBufferSize = value;
    }

    /**
     * assetCount 속성의 값을 가져옵니다.
     * 
     */
    public long getAssetCount() {
        return assetCount;
    }

    /**
     * assetCount 속성의 값을 설정합니다.
     * 
     */
    public void setAssetCount(long value) {
        this.assetCount = value;
    }

}
