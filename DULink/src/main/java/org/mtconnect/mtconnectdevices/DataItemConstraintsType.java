//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:02 AM KST 
//


package org.mtconnect.mtconnectdevices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         A set of limits for a data item
 *       
 * 
 * <p>DataItemConstraintsType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="DataItemConstraintsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;sequence>
 *             &lt;element name="Value" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemValueElementType" maxOccurs="unbounded"/>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="Minimum" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemValueType"/>
 *             &lt;element name="Maximum" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemValueType"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *         &lt;element name="Filter" type="{urn:mtconnect.org:MTConnectDevices:1.3}DataItemFilterType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataItemConstraintsType", propOrder = {
    "value",
    "minimum",
    "maximum",
    "filter"
})
public class DataItemConstraintsType {

    @XmlElement(name = "Value")
    protected List<DataItemValueElementType> value;
    @XmlElement(name = "Minimum")
    protected String minimum;
    @XmlElement(name = "Maximum")
    protected String maximum;
    @XmlElement(name = "Filter")
    protected DataItemFilterType filter;

    /**
     * Gets the value of the value property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the value property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataItemValueElementType }
     * 
     * 
     */
    public List<DataItemValueElementType> getValue() {
        if (value == null) {
            value = new ArrayList<DataItemValueElementType>();
        }
        return this.value;
    }

    /**
     * minimum 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimum() {
        return minimum;
    }

    /**
     * minimum 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimum(String value) {
        this.minimum = value;
    }

    /**
     * maximum 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximum() {
        return maximum;
    }

    /**
     * maximum 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximum(String value) {
        this.maximum = value;
    }

    /**
     * filter 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link DataItemFilterType }
     *     
     */
    public DataItemFilterType getFilter() {
        return filter;
    }

    /**
     * filter 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link DataItemFilterType }
     *     
     */
    public void setFilter(DataItemFilterType value) {
        this.filter = value;
    }

}
