//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:36 AM KST 
//


package org.mtconnect.mtconnectstreams;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         The stream of data for a component
 *       
 * 
 * <p>ComponentStreamType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="ComponentStreamType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Samples" type="{urn:mtconnect.org:MTConnectStreams:1.3}SamplesType" minOccurs="0"/>
 *         &lt;element name="Events" type="{urn:mtconnect.org:MTConnectStreams:1.3}EventsType" minOccurs="0"/>
 *         &lt;element name="Condition" type="{urn:mtconnect.org:MTConnectStreams:1.3}ConditionListType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="componentId" use="required" type="{urn:mtconnect.org:MTConnectStreams:1.3}ComponentIdType" />
 *       &lt;attribute name="name" type="{urn:mtconnect.org:MTConnectStreams:1.3}NameType" />
 *       &lt;attribute name="nativeName" type="{urn:mtconnect.org:MTConnectStreams:1.3}NameType" />
 *       &lt;attribute name="component" use="required" type="{urn:mtconnect.org:MTConnectStreams:1.3}NameType" />
 *       &lt;attribute name="uuid" type="{urn:mtconnect.org:MTConnectStreams:1.3}UuidType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentStreamType", propOrder = {
    "samples",
    "events",
    "condition"
})
public class ComponentStreamType {

    @XmlElement(name = "Samples")
    protected SamplesType samples;
    @XmlElement(name = "Events")
    protected EventsType events;
    @XmlElement(name = "Condition")
    protected ConditionListType condition;
    @XmlAttribute(name = "componentId", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    protected String componentId;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "nativeName")
    protected String nativeName;
    @XmlAttribute(name = "component", required = true)
    protected String component;
    @XmlAttribute(name = "uuid")
    protected String uuid;

    /**
     * samples 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link SamplesType }
     *     
     */
    public SamplesType getSamples() {
        return samples;
    }

    /**
     * samples 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link SamplesType }
     *     
     */
    public void setSamples(SamplesType value) {
        this.samples = value;
    }

    /**
     * events 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link EventsType }
     *     
     */
    public EventsType getEvents() {
        return events;
    }

    /**
     * events 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link EventsType }
     *     
     */
    public void setEvents(EventsType value) {
        this.events = value;
    }

    /**
     * condition 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ConditionListType }
     *     
     */
    public ConditionListType getCondition() {
        return condition;
    }

    /**
     * condition 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionListType }
     *     
     */
    public void setCondition(ConditionListType value) {
        this.condition = value;
    }

    /**
     * componentId 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponentId() {
        return componentId;
    }

    /**
     * componentId 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponentId(String value) {
        this.componentId = value;
    }

    /**
     * name 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * name 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * nativeName 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNativeName() {
        return nativeName;
    }

    /**
     * nativeName 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNativeName(String value) {
        this.nativeName = value;
    }

    /**
     * component 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComponent() {
        return component;
    }

    /**
     * component 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComponent(String value) {
        this.component = value;
    }

    /**
     * uuid 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * uuid 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

}
