//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:36 AM KST 
//


package org.mtconnect.mtconnectstreams;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         The condition of the device
 *       
 * 
 * <p>ConditionListType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="ConditionListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{urn:mtconnect.org:MTConnectStreams:1.3}Condition" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConditionListType", propOrder = {
    "condition"
})
public class ConditionListType {

    @XmlElementRef(name = "Condition", namespace = "urn:mtconnect.org:MTConnectStreams:1.3", type = JAXBElement.class)
    protected List<JAXBElement<? extends ConditionType>> condition;

    /**
     * Gets the value of the condition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the condition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link WarningType }{@code >}
     * {@link JAXBElement }{@code <}{@link UnavailableType }{@code >}
     * {@link JAXBElement }{@code <}{@link FaultType }{@code >}
     * {@link JAXBElement }{@code <}{@link NormalType }{@code >}
     * {@link JAXBElement }{@code <}{@link ConditionType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends ConditionType>> getCondition() {
        if (condition == null) {
            condition = new ArrayList<JAXBElement<? extends ConditionType>>();
        }
        return this.condition;
    }

}
