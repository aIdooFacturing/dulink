//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:36 AM KST 
//


package org.mtconnect.mtconnectstreams;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>ExecutionValueType에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * <p>
 * <pre>
 * &lt;simpleType name="ExecutionValueType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="READY"/>
 *     &lt;enumeration value="INTERRUPTED"/>
 *     &lt;enumeration value="ACTIVE"/>
 *     &lt;enumeration value="STOPPED"/>
 *     &lt;enumeration value="FEED_HOLD"/>
 *     &lt;enumeration value="PROGRAM_COMPLETED"/>
 *     &lt;enumeration value="PROGRAM_STOPPED"/>
 *     &lt;enumeration value="PROGRAM_OPTIONAL_STOP"/>
 *     &lt;enumeration value="UNAVAILABLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ExecutionValueType")
@XmlEnum
public enum ExecutionValueType {

    READY,
    INTERRUPTED,
    ACTIVE,
    STOPPED,
    FEED_HOLD,
    PROGRAM_COMPLETED,
    PROGRAM_STOPPED,
    PROGRAM_OPTIONAL_STOP,
    UNAVAILABLE;

    public String value() {
        return name();
    }

    public static ExecutionValueType fromValue(String v) {
        return valueOf(v);
    }

}
