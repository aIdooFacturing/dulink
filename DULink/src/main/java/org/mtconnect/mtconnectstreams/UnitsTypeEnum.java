//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:36 AM KST 
//


package org.mtconnect.mtconnectstreams;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>UnitsTypeEnum에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * <p>
 * <pre>
 * &lt;simpleType name="UnitsTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AMPERE"/>
 *     &lt;enumeration value="CELSIUS"/>
 *     &lt;enumeration value="COUNT"/>
 *     &lt;enumeration value="DEGREE"/>
 *     &lt;enumeration value="DEGREE/SECOND"/>
 *     &lt;enumeration value="DEGREE/SECOND^2"/>
 *     &lt;enumeration value="HERTZ"/>
 *     &lt;enumeration value="JOULE"/>
 *     &lt;enumeration value="KILOGRAM"/>
 *     &lt;enumeration value="LITER"/>
 *     &lt;enumeration value="LITER/SECOND"/>
 *     &lt;enumeration value="MILLIMETER"/>
 *     &lt;enumeration value="MILLIMETER/SECOND"/>
 *     &lt;enumeration value="MILLIMETER/SECOND^2"/>
 *     &lt;enumeration value="MILLIMETER_3D"/>
 *     &lt;enumeration value="NEWTON"/>
 *     &lt;enumeration value="NEWTON_METER"/>
 *     &lt;enumeration value="PASCAL"/>
 *     &lt;enumeration value="PERCENT"/>
 *     &lt;enumeration value="PH"/>
 *     &lt;enumeration value="REVOLUTION/MINUTE"/>
 *     &lt;enumeration value="SECOND"/>
 *     &lt;enumeration value="VOLT"/>
 *     &lt;enumeration value="WATT"/>
 *     &lt;enumeration value="OHM"/>
 *     &lt;enumeration value="SOUND_LEVEL"/>
 *     &lt;enumeration value="SIEMENS/METER"/>
 *     &lt;enumeration value="MICRO_RADIAN"/>
 *     &lt;enumeration value="PASCAL_SECOND"/>
 *     &lt;enumeration value="VOLT_AMPERE"/>
 *     &lt;enumeration value="VOLT_AMPERE_REACTIVE"/>
 *     &lt;enumeration value="WATT_SECOND"/>
 *     &lt;enumeration value="DECIBEL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UnitsTypeEnum")
@XmlEnum
public enum UnitsTypeEnum {

    AMPERE("AMPERE"),
    CELSIUS("CELSIUS"),
    COUNT("COUNT"),
    DEGREE("DEGREE"),
    @XmlEnumValue("DEGREE/SECOND")
    DEGREE_SECOND("DEGREE/SECOND"),
    @XmlEnumValue("DEGREE/SECOND^2")
    DEGREE_SECOND_2("DEGREE/SECOND^2"),
    HERTZ("HERTZ"),
    JOULE("JOULE"),
    KILOGRAM("KILOGRAM"),
    LITER("LITER"),
    @XmlEnumValue("LITER/SECOND")
    LITER_SECOND("LITER/SECOND"),
    MILLIMETER("MILLIMETER"),
    @XmlEnumValue("MILLIMETER/SECOND")
    MILLIMETER_SECOND("MILLIMETER/SECOND"),
    @XmlEnumValue("MILLIMETER/SECOND^2")
    MILLIMETER_SECOND_2("MILLIMETER/SECOND^2"),
    @XmlEnumValue("MILLIMETER_3D")
    MILLIMETER_3_D("MILLIMETER_3D"),
    NEWTON("NEWTON"),
    NEWTON_METER("NEWTON_METER"),
    PASCAL("PASCAL"),
    PERCENT("PERCENT"),
    PH("PH"),
    @XmlEnumValue("REVOLUTION/MINUTE")
    REVOLUTION_MINUTE("REVOLUTION/MINUTE"),
    SECOND("SECOND"),
    VOLT("VOLT"),
    WATT("WATT"),
    OHM("OHM"),
    SOUND_LEVEL("SOUND_LEVEL"),
    @XmlEnumValue("SIEMENS/METER")
    SIEMENS_METER("SIEMENS/METER"),
    MICRO_RADIAN("MICRO_RADIAN"),
    PASCAL_SECOND("PASCAL_SECOND"),
    VOLT_AMPERE("VOLT_AMPERE"),
    VOLT_AMPERE_REACTIVE("VOLT_AMPERE_REACTIVE"),
    WATT_SECOND("WATT_SECOND"),
    DECIBEL("DECIBEL");
    private final String value;

    UnitsTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnitsTypeEnum fromValue(String v) {
        for (UnitsTypeEnum c: UnitsTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
