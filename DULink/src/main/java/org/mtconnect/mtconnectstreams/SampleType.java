//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:36 AM KST 
//


package org.mtconnect.mtconnectstreams;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         An abstract sample
 *       
 * 
 * <p>SampleType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="SampleType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;urn:mtconnect.org:MTConnectStreams:1.3>ResultType">
 *       &lt;attribute name="sampleRate" type="{urn:mtconnect.org:MTConnectStreams:1.3}SampleRateType" />
 *       &lt;attribute name="statistic" type="{urn:mtconnect.org:MTConnectStreams:1.3}DataItemStatisticsType" />
 *       &lt;attribute name="duration" type="{urn:mtconnect.org:MTConnectStreams:1.3}DurationTimeType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SampleType")
@XmlSeeAlso({
    AmperageType.class,
    LoadType.class,
    PathPositionType.class,
    PositionType.class,
    SoundPressureType.class,
    RotaryVelocityType.class,
    PathFeedrateType.class,
    ConductivityType.class,
    ConcentrationType.class,
    FlowType.class,
    GlobalPositionType.class,
    AngleType.class,
    LengthType.class,
    FrequencyType.class,
    AngularVelocityType.class,
    AccelerationType.class,
    FillLevelType.class,
    TorqueType.class,
    WattageType.class,
    VoltsType.class,
    AngularAccelerationType.class,
    PressureType.class,
    SpindleSpeedType.class,
    StrainType.class,
    ResistanceType.class,
    LinearForceType.class,
    ElectricalEnergyType.class,
    ViscosityType.class,
    TemperatureType.class,
    AccumulatedTimeType.class,
    WattType.class,
    VoltageType.class,
    PowerFactorType.class,
    VelocityType.class,
    DisplacementType.class,
    TiltType.class,
    AbsTimeSeriesType.class,
    AxisFeedrateType.class
})
public abstract class SampleType
    extends ResultType
{

    @XmlAttribute(name = "sampleRate")
    protected String sampleRate;
    @XmlAttribute(name = "statistic")
    protected String statistic;
    @XmlAttribute(name = "duration")
    protected Float duration;

    /**
     * sampleRate 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSampleRate() {
        return sampleRate;
    }

    /**
     * sampleRate 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSampleRate(String value) {
        this.sampleRate = value;
    }

    /**
     * statistic 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatistic() {
        return statistic;
    }

    /**
     * statistic 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatistic(String value) {
        this.statistic = value;
    }

    /**
     * duration 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getDuration() {
        return duration;
    }

    /**
     * duration 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setDuration(Float value) {
        this.duration = value;
    }

}
