//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:36 AM KST 
//


package org.mtconnect.mtconnectstreams;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         An abstract event
 *       
 * 
 * <p>EventType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="EventType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;urn:mtconnect.org:MTConnectStreams:1.3>ResultType">
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventType")
@XmlSeeAlso({
    EndOfBarType.class,
    PalletIdType.class,
    ProgramEditType.class,
    PathFeedrateOverrideType.class,
    ProgramHeaderType.class,
    CodeType.class,
    AxesCouplingType.class,
    ChuckStateType.class,
    LineType.class,
    AxisStateType.class,
    InterfaceStateType.class,
    ProgramCommentType.class,
    DirectionType.class,
    CoupledAxesType.class,
    RotaryModeType.class,
    AssetRemovedType.class,
    WorkholdingIdType.class,
    ControllerModeType.class,
    PowerStateType.class,
    ToolAssetIdType.class,
    MessageType.class,
    BlockType.class,
    DoorStateType.class,
    ExecutionType.class,
    PowerStatusType.class,
    PartAssetIdType.class,
    PartIdType.class,
    AvailabilityType.class,
    ProgramType.class,
    ToolNumberType.class,
    FunctionalModeType.class,
    ActuatorStateType.class,
    InterfaceEventType.class,
    PartCountType.class,
    PathModeType.class,
    EmergencyStopType.class,
    ToolIdType.class,
    AssetChangedType.class,
    RotaryVelocityOverrideType.class,
    ChuckInterlockType.class,
    OperatorIdType.class,
    AxisFeedrateOverrideType.class,
    ActiveAxesType.class,
    AlarmType.class,
    ProgramEditNameType.class,
    AxisInterlockType.class
})
public abstract class EventType
    extends ResultType
{


}
