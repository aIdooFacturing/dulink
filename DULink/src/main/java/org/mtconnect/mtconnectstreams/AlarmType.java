//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:31:36 AM KST 
//


package org.mtconnect.mtconnectstreams;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         DEPRECATED: An Notifcation event
 *       
 * 
 * <p>AlarmType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="AlarmType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;urn:mtconnect.org:MTConnectStreams:1.3>EventType">
 *       &lt;attribute name="code" use="required" type="{urn:mtconnect.org:MTConnectStreams:1.3}NotifcationCodeType" />
 *       &lt;attribute name="severity" type="{urn:mtconnect.org:MTConnectStreams:1.3}SeverityType" />
 *       &lt;attribute name="state" type="{urn:mtconnect.org:MTConnectStreams:1.3}AlarmStateType" />
 *       &lt;attribute name="nativeCode" use="required" type="{urn:mtconnect.org:MTConnectStreams:1.3}NativeNotifcationCodeType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlarmType")
public class AlarmType
    extends EventType
{

    @XmlAttribute(name = "code", required = true)
    protected NotifcationCodeType code;
    @XmlAttribute(name = "severity")
    protected SeverityType severity;
    @XmlAttribute(name = "state")
    protected AlarmStateType state;
    @XmlAttribute(name = "nativeCode", required = true)
    protected String nativeCode;

    /**
     * code 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link NotifcationCodeType }
     *     
     */
    public NotifcationCodeType getCode() {
        return code;
    }

    /**
     * code 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link NotifcationCodeType }
     *     
     */
    public void setCode(NotifcationCodeType value) {
        this.code = value;
    }

    /**
     * severity 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link SeverityType }
     *     
     */
    public SeverityType getSeverity() {
        return severity;
    }

    /**
     * severity 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link SeverityType }
     *     
     */
    public void setSeverity(SeverityType value) {
        this.severity = value;
    }

    /**
     * state 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link AlarmStateType }
     *     
     */
    public AlarmStateType getState() {
        return state;
    }

    /**
     * state 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link AlarmStateType }
     *     
     */
    public void setState(AlarmStateType value) {
        this.state = value;
    }

    /**
     * nativeCode 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNativeCode() {
        return nativeCode;
    }

    /**
     * nativeCode 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNativeCode(String value) {
        this.nativeCode = value;
    }

}
