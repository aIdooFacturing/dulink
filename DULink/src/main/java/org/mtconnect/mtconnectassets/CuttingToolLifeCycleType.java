//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         A defintion of a cutting tool application and life cycle
 *       
 * 
 * <p>CuttingToolLifeCycleType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="CuttingToolLifeCycleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CutterStatus" type="{urn:mtconnect.org:MTConnectAssets:1.3}CutterStatusType"/>
 *         &lt;element name="ReconditionCount" type="{urn:mtconnect.org:MTConnectAssets:1.3}ReconditionCountType" minOccurs="0"/>
 *         &lt;element name="ToolLife" type="{urn:mtconnect.org:MTConnectAssets:1.3}LifeType" maxOccurs="3" minOccurs="0"/>
 *         &lt;element name="ProgramToolGroup" type="{urn:mtconnect.org:MTConnectAssets:1.3}ProgramToolGroupType" minOccurs="0"/>
 *         &lt;element name="ProgramToolNumber" type="{urn:mtconnect.org:MTConnectAssets:1.3}ProgramToolNumberType" minOccurs="0"/>
 *         &lt;element name="Location" type="{urn:mtconnect.org:MTConnectAssets:1.3}LocationType" minOccurs="0"/>
 *         &lt;element name="ProcessSpindleSpeed" type="{urn:mtconnect.org:MTConnectAssets:1.3}ProcessSpindleSpeedType" minOccurs="0"/>
 *         &lt;element name="ProcessFeedRate" type="{urn:mtconnect.org:MTConnectAssets:1.3}ProcessFeedRateType" minOccurs="0"/>
 *         &lt;element name="ConnectionCodeMachineSide" type="{urn:mtconnect.org:MTConnectAssets:1.3}ConnectionCodeMachineSideType" minOccurs="0"/>
 *         &lt;element name="Measurements" type="{urn:mtconnect.org:MTConnectAssets:1.3}AssemblyMeasurementsType" minOccurs="0"/>
 *         &lt;element name="CuttingItems" type="{urn:mtconnect.org:MTConnectAssets:1.3}CuttingItemsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CuttingToolLifeCycleType", propOrder = {
    "cutterStatus",
    "reconditionCount",
    "toolLife",
    "programToolGroup",
    "programToolNumber",
    "location",
    "processSpindleSpeed",
    "processFeedRate",
    "connectionCodeMachineSide",
    "measurements",
    "cuttingItems"
})
public class CuttingToolLifeCycleType {

    @XmlElement(name = "CutterStatus", required = true)
    protected CutterStatusType cutterStatus;
    @XmlElement(name = "ReconditionCount")
    protected ReconditionCountType reconditionCount;
    @XmlElement(name = "ToolLife")
    protected List<LifeType> toolLife;
    @XmlElement(name = "ProgramToolGroup")
    protected String programToolGroup;
    @XmlElement(name = "ProgramToolNumber")
    protected BigInteger programToolNumber;
    @XmlElement(name = "Location")
    protected LocationType location;
    @XmlElement(name = "ProcessSpindleSpeed")
    protected ProcessSpindleSpeedType processSpindleSpeed;
    @XmlElement(name = "ProcessFeedRate")
    protected ProcessFeedRateType processFeedRate;
    @XmlElement(name = "ConnectionCodeMachineSide")
    protected String connectionCodeMachineSide;
    @XmlElement(name = "Measurements")
    protected AssemblyMeasurementsType measurements;
    @XmlElement(name = "CuttingItems")
    protected CuttingItemsType cuttingItems;

    /**
     * cutterStatus 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link CutterStatusType }
     *     
     */
    public CutterStatusType getCutterStatus() {
        return cutterStatus;
    }

    /**
     * cutterStatus 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link CutterStatusType }
     *     
     */
    public void setCutterStatus(CutterStatusType value) {
        this.cutterStatus = value;
    }

    /**
     * reconditionCount 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ReconditionCountType }
     *     
     */
    public ReconditionCountType getReconditionCount() {
        return reconditionCount;
    }

    /**
     * reconditionCount 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ReconditionCountType }
     *     
     */
    public void setReconditionCount(ReconditionCountType value) {
        this.reconditionCount = value;
    }

    /**
     * Gets the value of the toolLife property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the toolLife property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getToolLife().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LifeType }
     * 
     * 
     */
    public List<LifeType> getToolLife() {
        if (toolLife == null) {
            toolLife = new ArrayList<LifeType>();
        }
        return this.toolLife;
    }

    /**
     * programToolGroup 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgramToolGroup() {
        return programToolGroup;
    }

    /**
     * programToolGroup 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgramToolGroup(String value) {
        this.programToolGroup = value;
    }

    /**
     * programToolNumber 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getProgramToolNumber() {
        return programToolNumber;
    }

    /**
     * programToolNumber 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setProgramToolNumber(BigInteger value) {
        this.programToolNumber = value;
    }

    /**
     * location 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getLocation() {
        return location;
    }

    /**
     * location 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setLocation(LocationType value) {
        this.location = value;
    }

    /**
     * processSpindleSpeed 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ProcessSpindleSpeedType }
     *     
     */
    public ProcessSpindleSpeedType getProcessSpindleSpeed() {
        return processSpindleSpeed;
    }

    /**
     * processSpindleSpeed 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessSpindleSpeedType }
     *     
     */
    public void setProcessSpindleSpeed(ProcessSpindleSpeedType value) {
        this.processSpindleSpeed = value;
    }

    /**
     * processFeedRate 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link ProcessFeedRateType }
     *     
     */
    public ProcessFeedRateType getProcessFeedRate() {
        return processFeedRate;
    }

    /**
     * processFeedRate 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessFeedRateType }
     *     
     */
    public void setProcessFeedRate(ProcessFeedRateType value) {
        this.processFeedRate = value;
    }

    /**
     * connectionCodeMachineSide 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConnectionCodeMachineSide() {
        return connectionCodeMachineSide;
    }

    /**
     * connectionCodeMachineSide 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConnectionCodeMachineSide(String value) {
        this.connectionCodeMachineSide = value;
    }

    /**
     * measurements 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link AssemblyMeasurementsType }
     *     
     */
    public AssemblyMeasurementsType getMeasurements() {
        return measurements;
    }

    /**
     * measurements 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link AssemblyMeasurementsType }
     *     
     */
    public void setMeasurements(AssemblyMeasurementsType value) {
        this.measurements = value;
    }

    /**
     * cuttingItems 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link CuttingItemsType }
     *     
     */
    public CuttingItemsType getCuttingItems() {
        return cuttingItems;
    }

    /**
     * cuttingItems 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link CuttingItemsType }
     *     
     */
    public void setCuttingItems(CuttingItemsType value) {
        this.cuttingItems = value;
    }

}
