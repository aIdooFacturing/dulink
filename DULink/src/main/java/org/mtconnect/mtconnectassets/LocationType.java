//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * 
 *         The location of the tool in the tool changer (pot) or the station of the
 *         tool
 *       
 * 
 * <p>LocationType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="LocationType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;urn:mtconnect.org:MTConnectAssets:1.3>LocationValueType">
 *       &lt;attribute name="type" use="required" type="{urn:mtconnect.org:MTConnectAssets:1.3}LocationsType" />
 *       &lt;attribute name="negativeOverlap" use="required" type="{urn:mtconnect.org:MTConnectAssets:1.3}OverlapType" />
 *       &lt;attribute name="positiveOverlap" use="required" type="{urn:mtconnect.org:MTConnectAssets:1.3}OverlapType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationType", propOrder = {
    "value"
})
public class LocationType {

    @XmlValue
    protected BigInteger value;
    @XmlAttribute(name = "type", required = true)
    protected LocationsType type;
    @XmlAttribute(name = "negativeOverlap", required = true)
    protected BigInteger negativeOverlap;
    @XmlAttribute(name = "positiveOverlap", required = true)
    protected BigInteger positiveOverlap;

    /**
     * 
     *         The tool location
     *       
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getValue() {
        return value;
    }

    /**
     * value 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setValue(BigInteger value) {
        this.value = value;
    }

    /**
     * type 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link LocationsType }
     *     
     */
    public LocationsType getType() {
        return type;
    }

    /**
     * type 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationsType }
     *     
     */
    public void setType(LocationsType value) {
        this.type = value;
    }

    /**
     * negativeOverlap 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNegativeOverlap() {
        return negativeOverlap;
    }

    /**
     * negativeOverlap 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNegativeOverlap(BigInteger value) {
        this.negativeOverlap = value;
    }

    /**
     * positiveOverlap 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPositiveOverlap() {
        return positiveOverlap;
    }

    /**
     * positiveOverlap 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPositiveOverlap(BigInteger value) {
        this.positiveOverlap = value;
    }

}
