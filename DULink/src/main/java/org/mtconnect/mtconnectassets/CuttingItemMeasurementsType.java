//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         A collection of assembly measurements
 *       
 * 
 * <p>CuttingItemMeasurementsType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="CuttingItemMeasurementsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{urn:mtconnect.org:MTConnectAssets:1.3}CommonMeasurement"/>
 *         &lt;element ref="{urn:mtconnect.org:MTConnectAssets:1.3}CuttingItemMeasurement"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CuttingItemMeasurementsType", propOrder = {
    "commonMeasurementOrCuttingItemMeasurement"
})
public class CuttingItemMeasurementsType {

    @XmlElementRefs({
        @XmlElementRef(name = "CommonMeasurement", namespace = "urn:mtconnect.org:MTConnectAssets:1.3", type = JAXBElement.class),
        @XmlElementRef(name = "CuttingItemMeasurement", namespace = "urn:mtconnect.org:MTConnectAssets:1.3", type = JAXBElement.class)
    })
    protected List<JAXBElement<? extends MeasurementType>> commonMeasurementOrCuttingItemMeasurement;

    /**
     * Gets the value of the commonMeasurementOrCuttingItemMeasurement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commonMeasurementOrCuttingItemMeasurement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommonMeasurementOrCuttingItemMeasurement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link FunctionalLengthType }{@code >}
     * {@link JAXBElement }{@code <}{@link WiperEdgeLengthType }{@code >}
     * {@link JAXBElement }{@code <}{@link CommonMeasurementType }{@code >}
     * {@link JAXBElement }{@code <}{@link StepIncludedAngleType }{@code >}
     * {@link JAXBElement }{@code <}{@link IncribedCircleDiameterType }{@code >}
     * {@link JAXBElement }{@code <}{@link CuttingReferencePointType }{@code >}
     * {@link JAXBElement }{@code <}{@link CuttingDiameterType }{@code >}
     * {@link JAXBElement }{@code <}{@link ProtrudingLengthType }{@code >}
     * {@link JAXBElement }{@code <}{@link PointAngleType }{@code >}
     * {@link JAXBElement }{@code <}{@link FunctionalWidthType }{@code >}
     * {@link JAXBElement }{@code <}{@link CornerRadiusType }{@code >}
     * {@link JAXBElement }{@code <}{@link InclinationAngleType }{@code >}
     * {@link JAXBElement }{@code <}{@link ToolLeadAngleType }{@code >}
     * {@link JAXBElement }{@code <}{@link ToolCuttingEdgeAngleType }{@code >}
     * {@link JAXBElement }{@code <}{@link CuttingItemMeasurementType }{@code >}
     * {@link JAXBElement }{@code <}{@link CuttingHeightType }{@code >}
     * {@link JAXBElement }{@code <}{@link FlangeDiameterType }{@code >}
     * {@link JAXBElement }{@code <}{@link CuttingEdgeLengthType }{@code >}
     * {@link JAXBElement }{@code <}{@link WeightType }{@code >}
     * {@link JAXBElement }{@code <}{@link StepDiameterLengthType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends MeasurementType>> getCommonMeasurementOrCuttingItemMeasurement() {
        if (commonMeasurementOrCuttingItemMeasurement == null) {
            commonMeasurementOrCuttingItemMeasurement = new ArrayList<JAXBElement<? extends MeasurementType>>();
        }
        return this.commonMeasurementOrCuttingItemMeasurement;
    }

}
