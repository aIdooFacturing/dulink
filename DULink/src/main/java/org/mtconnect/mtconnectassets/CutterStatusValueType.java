//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>CutterStatusValueType에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * <p>
 * <pre>
 * &lt;simpleType name="CutterStatusValueType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NEW"/>
 *     &lt;enumeration value="AVAILABLE"/>
 *     &lt;enumeration value="UNAVAILABLE"/>
 *     &lt;enumeration value="ALLOCATED"/>
 *     &lt;enumeration value="UNALLOCATED"/>
 *     &lt;enumeration value="MEASURED"/>
 *     &lt;enumeration value="NOT_REGISTERED"/>
 *     &lt;enumeration value="RECONDITIONED"/>
 *     &lt;enumeration value="USED"/>
 *     &lt;enumeration value="EXPIRED"/>
 *     &lt;enumeration value="TAGGED_OUT"/>
 *     &lt;enumeration value="BROKEN"/>
 *     &lt;enumeration value="UNKNOWN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CutterStatusValueType")
@XmlEnum
public enum CutterStatusValueType {

    NEW,
    AVAILABLE,
    UNAVAILABLE,
    ALLOCATED,
    UNALLOCATED,
    MEASURED,
    NOT_REGISTERED,
    RECONDITIONED,
    USED,
    EXPIRED,
    TAGGED_OUT,
    BROKEN,
    UNKNOWN;

    public String value() {
        return name();
    }

    public static CutterStatusValueType fromValue(String v) {
        return valueOf(v);
    }

}
