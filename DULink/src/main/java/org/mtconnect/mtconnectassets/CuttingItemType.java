//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         An edge into a tool assembly
 *       
 * 
 * <p>CuttingItemType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="CuttingItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{urn:mtconnect.org:MTConnectAssets:1.3}AssetDescriptionType" minOccurs="0"/>
 *         &lt;element name="Locus" type="{urn:mtconnect.org:MTConnectAssets:1.3}LocusType" minOccurs="0"/>
 *         &lt;element name="ItemLife" type="{urn:mtconnect.org:MTConnectAssets:1.3}LifeType" maxOccurs="3" minOccurs="0"/>
 *         &lt;element name="Measurements" type="{urn:mtconnect.org:MTConnectAssets:1.3}CuttingItemMeasurementsType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="indices" use="required" type="{urn:mtconnect.org:MTConnectAssets:1.3}IndexRangeType" />
 *       &lt;attribute name="itemId" type="{urn:mtconnect.org:MTConnectAssets:1.3}ItemIdType" />
 *       &lt;attribute name="grade" type="{urn:mtconnect.org:MTConnectAssets:1.3}GradeType" />
 *       &lt;attribute name="manufacturers" type="{urn:mtconnect.org:MTConnectAssets:1.3}ManufacturersType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CuttingItemType", propOrder = {
    "description",
    "locus",
    "itemLife",
    "measurements"
})
public class CuttingItemType {

    @XmlElement(name = "Description")
    protected AssetDescriptionType description;
    @XmlElement(name = "Locus")
    protected String locus;
    @XmlElement(name = "ItemLife")
    protected List<LifeType> itemLife;
    @XmlElement(name = "Measurements")
    protected CuttingItemMeasurementsType measurements;
    @XmlAttribute(name = "indices", required = true)
    protected String indices;
    @XmlAttribute(name = "itemId")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemId;
    @XmlAttribute(name = "grade")
    protected String grade;
    @XmlAttribute(name = "manufacturers")
    protected String manufacturers;

    /**
     * description 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link AssetDescriptionType }
     *     
     */
    public AssetDescriptionType getDescription() {
        return description;
    }

    /**
     * description 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link AssetDescriptionType }
     *     
     */
    public void setDescription(AssetDescriptionType value) {
        this.description = value;
    }

    /**
     * locus 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocus() {
        return locus;
    }

    /**
     * locus 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocus(String value) {
        this.locus = value;
    }

    /**
     * Gets the value of the itemLife property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemLife property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemLife().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LifeType }
     * 
     * 
     */
    public List<LifeType> getItemLife() {
        if (itemLife == null) {
            itemLife = new ArrayList<LifeType>();
        }
        return this.itemLife;
    }

    /**
     * measurements 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link CuttingItemMeasurementsType }
     *     
     */
    public CuttingItemMeasurementsType getMeasurements() {
        return measurements;
    }

    /**
     * measurements 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link CuttingItemMeasurementsType }
     *     
     */
    public void setMeasurements(CuttingItemMeasurementsType value) {
        this.measurements = value;
    }

    /**
     * indices 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndices() {
        return indices;
    }

    /**
     * indices 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndices(String value) {
        this.indices = value;
    }

    /**
     * itemId 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * itemId 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemId(String value) {
        this.itemId = value;
    }

    /**
     * grade 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrade() {
        return grade;
    }

    /**
     * grade 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrade(String value) {
        this.grade = value;
    }

    /**
     * manufacturers 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturers() {
        return manufacturers;
    }

    /**
     * manufacturers 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturers(String value) {
        this.manufacturers = value;
    }

}
