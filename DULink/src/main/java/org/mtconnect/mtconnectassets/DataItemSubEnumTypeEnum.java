//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>DataItemSubEnumTypeEnum에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * <p>
 * <pre>
 * &lt;simpleType name="DataItemSubEnumTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACTUAL"/>
 *     &lt;enumeration value="COMMANDED"/>
 *     &lt;enumeration value="MAXIMUM"/>
 *     &lt;enumeration value="MINIMUM"/>
 *     &lt;enumeration value="OTHER"/>
 *     &lt;enumeration value="OVERRIDE"/>
 *     &lt;enumeration value="PROBE"/>
 *     &lt;enumeration value="TARGET"/>
 *     &lt;enumeration value="GOOD"/>
 *     &lt;enumeration value="BAD"/>
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="LINE"/>
 *     &lt;enumeration value="CONTROL"/>
 *     &lt;enumeration value="ALTERNATING"/>
 *     &lt;enumeration value="DIRECT"/>
 *     &lt;enumeration value="WEIGHT"/>
 *     &lt;enumeration value="VOLUME"/>
 *     &lt;enumeration value="MOLE"/>
 *     &lt;enumeration value="KINETIC"/>
 *     &lt;enumeration value="DYNAMIC"/>
 *     &lt;enumeration value="NO_SCALE"/>
 *     &lt;enumeration value="A_SCALE"/>
 *     &lt;enumeration value="B_SCALE"/>
 *     &lt;enumeration value="C_SCALE"/>
 *     &lt;enumeration value="D_SCALE"/>
 *     &lt;enumeration value="REQUEST"/>
 *     &lt;enumeration value="RESPONSE"/>
 *     &lt;enumeration value="REMAINING"/>
 *     &lt;enumeration value="JOG"/>
 *     &lt;enumeration value="RAPID"/>
 *     &lt;enumeration value="PROGRAMMED"/>
 *     &lt;enumeration value="PRIMARY"/>
 *     &lt;enumeration value="AUXILIARY"/>
 *     &lt;enumeration value="MANUAL_UNCLAMP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DataItemSubEnumTypeEnum")
@XmlEnum
public enum DataItemSubEnumTypeEnum {

    ACTUAL,
    COMMANDED,
    MAXIMUM,
    MINIMUM,
    OTHER,
    OVERRIDE,
    PROBE,
    TARGET,
    GOOD,
    BAD,
    ALL,
    LINE,
    CONTROL,
    ALTERNATING,
    DIRECT,
    WEIGHT,
    VOLUME,
    MOLE,
    KINETIC,
    DYNAMIC,
    NO_SCALE,
    A_SCALE,
    B_SCALE,
    C_SCALE,
    D_SCALE,
    REQUEST,
    RESPONSE,
    REMAINING,
    JOG,
    RAPID,
    PROGRAMMED,
    PRIMARY,
    AUXILIARY,
    MANUAL_UNCLAMP;

    public String value() {
        return name();
    }

    public static DataItemSubEnumTypeEnum fromValue(String v) {
        return valueOf(v);
    }

}
