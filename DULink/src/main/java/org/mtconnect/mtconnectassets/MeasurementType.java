//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * 
 *         An abstract type for edge measurements
 *       
 * 
 * <p>MeasurementType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="MeasurementType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;urn:mtconnect.org:MTConnectAssets:1.3>MeasurementValueType">
 *       &lt;attribute name="significantDigits" type="{urn:mtconnect.org:MTConnectAssets:1.3}SignificantDigitsValueType" />
 *       &lt;attribute name="units" type="{urn:mtconnect.org:MTConnectAssets:1.3}UnitsType" />
 *       &lt;attribute name="nativeUnits" type="{urn:mtconnect.org:MTConnectAssets:1.3}NativeUnitsType" />
 *       &lt;attribute name="code" type="{urn:mtconnect.org:MTConnectAssets:1.3}CodeType" />
 *       &lt;attribute name="maximum" type="{urn:mtconnect.org:MTConnectAssets:1.3}MeasurementValueType" />
 *       &lt;attribute name="minimum" type="{urn:mtconnect.org:MTConnectAssets:1.3}MeasurementValueType" />
 *       &lt;attribute name="nominal" type="{urn:mtconnect.org:MTConnectAssets:1.3}MeasurementValueType" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeasurementType", propOrder = {
    "value"
})
@XmlSeeAlso({
    AssemblyMeasurementType.class,
    CommonMeasurementType.class,
    CuttingItemMeasurementType.class
})
public abstract class MeasurementType {

    @XmlValue
    protected float value;
    @XmlAttribute(name = "significantDigits")
    protected BigInteger significantDigits;
    @XmlAttribute(name = "units")
    protected String units;
    @XmlAttribute(name = "nativeUnits")
    protected String nativeUnits;
    @XmlAttribute(name = "code")
    protected String code;
    @XmlAttribute(name = "maximum")
    protected Float maximum;
    @XmlAttribute(name = "minimum")
    protected Float minimum;
    @XmlAttribute(name = "nominal")
    protected Float nominal;

    /**
     * 
     *         A measurement value
     *       
     * 
     */
    public float getValue() {
        return value;
    }

    /**
     * value 속성의 값을 설정합니다.
     * 
     */
    public void setValue(float value) {
        this.value = value;
    }

    /**
     * significantDigits 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSignificantDigits() {
        return significantDigits;
    }

    /**
     * significantDigits 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSignificantDigits(BigInteger value) {
        this.significantDigits = value;
    }

    /**
     * units 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnits() {
        return units;
    }

    /**
     * units 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnits(String value) {
        this.units = value;
    }

    /**
     * nativeUnits 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNativeUnits() {
        return nativeUnits;
    }

    /**
     * nativeUnits 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNativeUnits(String value) {
        this.nativeUnits = value;
    }

    /**
     * code 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * code 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * maximum 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getMaximum() {
        return maximum;
    }

    /**
     * maximum 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setMaximum(Float value) {
        this.maximum = value;
    }

    /**
     * minimum 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getMinimum() {
        return minimum;
    }

    /**
     * minimum 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setMinimum(Float value) {
        this.minimum = value;
    }

    /**
     * nominal 속성의 값을 가져옵니다.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getNominal() {
        return nominal;
    }

    /**
     * nominal 속성의 값을 설정합니다.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setNominal(Float value) {
        this.nominal = value;
    }

}
