//
// 이 파일은 JAXB(JavaTM Architecture for XML Binding) 참조 구현 2.2.8-b130911.1802 버전을 통해 생성되었습니다. 
// <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>를 참조하십시오. 
// 이 파일을 수정하면 소스 스키마를 재컴파일할 때 수정 사항이 손실됩니다. 
// 생성 날짜: 2015.03.31 시간 08:28:27 AM KST 
//


package org.mtconnect.mtconnectassets;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         A collection of assembly measurements
 *       
 * 
 * <p>AssemblyMeasurementsType complex type에 대한 Java 클래스입니다.
 * 
 * <p>다음 스키마 단편이 이 클래스에 포함되는 필요한 콘텐츠를 지정합니다.
 * 
 * <pre>
 * &lt;complexType name="AssemblyMeasurementsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded">
 *         &lt;element ref="{urn:mtconnect.org:MTConnectAssets:1.3}CommonMeasurement"/>
 *         &lt;element ref="{urn:mtconnect.org:MTConnectAssets:1.3}AssemblyMeasurement"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssemblyMeasurementsType", propOrder = {
    "commonMeasurementOrAssemblyMeasurement"
})
public class AssemblyMeasurementsType {

    @XmlElementRefs({
        @XmlElementRef(name = "AssemblyMeasurement", namespace = "urn:mtconnect.org:MTConnectAssets:1.3", type = JAXBElement.class),
        @XmlElementRef(name = "CommonMeasurement", namespace = "urn:mtconnect.org:MTConnectAssets:1.3", type = JAXBElement.class)
    })
    protected List<JAXBElement<? extends MeasurementType>> commonMeasurementOrAssemblyMeasurement;

    /**
     * Gets the value of the commonMeasurementOrAssemblyMeasurement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commonMeasurementOrAssemblyMeasurement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommonMeasurementOrAssemblyMeasurement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link FunctionalLengthType }{@code >}
     * {@link JAXBElement }{@code <}{@link AssemblyMeasurementType }{@code >}
     * {@link JAXBElement }{@code <}{@link CommonMeasurementType }{@code >}
     * {@link JAXBElement }{@code <}{@link UsableLengthMaxType }{@code >}
     * {@link JAXBElement }{@code <}{@link FlangeDiameterMaxType }{@code >}
     * {@link JAXBElement }{@code <}{@link OverallToolLengthType }{@code >}
     * {@link JAXBElement }{@code <}{@link CuttingDiameterMaxType }{@code >}
     * {@link JAXBElement }{@code <}{@link BodyDiameterMaxType }{@code >}
     * {@link JAXBElement }{@code <}{@link DepthOfCutMaxType }{@code >}
     * {@link JAXBElement }{@code <}{@link ProtrudingLengthType }{@code >}
     * {@link JAXBElement }{@code <}{@link ShankDiameterType }{@code >}
     * {@link JAXBElement }{@code <}{@link ShankLengthType }{@code >}
     * {@link JAXBElement }{@code <}{@link BodyLengthMaxType }{@code >}
     * {@link JAXBElement }{@code <}{@link ShankHeightType }{@code >}
     * {@link JAXBElement }{@code <}{@link WeightType }{@code >}
     * 
     * 
     */
    public List<JAXBElement<? extends MeasurementType>> getCommonMeasurementOrAssemblyMeasurement() {
        if (commonMeasurementOrAssemblyMeasurement == null) {
            commonMeasurementOrAssemblyMeasurement = new ArrayList<JAXBElement<? extends MeasurementType>>();
        }
        return this.commonMeasurementOrAssemblyMeasurement;
    }

}
