package com.unomic.dulink.mes.service;

import java.util.List;

import com.unomic.dulink.mes.domain.MesVo;


public interface MesService {
	
	public List<MesVo> getListEpps();	
};
