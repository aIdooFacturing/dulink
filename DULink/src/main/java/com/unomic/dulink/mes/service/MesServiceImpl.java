package com.unomic.dulink.mes.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.mes.domain.MesVo;


@Service
@Repository
public class MesServiceImpl extends SqlSessionDaoSupport implements MesService{
	private final static String MES_SPACE= "com.dulink.mes.";
	
	@Override
	public List <MesVo> getListEpps()
	{
		SqlSession sql = getSqlSession();
		List <MesVo> lisEpps=  sql.selectList(MES_SPACE + "getListEPPS");
		
		return lisEpps;
	}
	
	

	
}