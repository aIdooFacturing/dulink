package com.unomic.dulink.mes.controller;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.mes.domain.MesVo;
import com.unomic.dulink.mes.service.MesService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/mes")
@Controller
public class MesController {

	private static final Logger logger = LoggerFactory.getLogger(MesController.class);
	
	@Autowired
	MesService mesService;
	
	@RequestMapping(value="mesTest")
	@ResponseBody
	public List<MesVo> adjustDeviceStatus(){
		List<MesVo> listEpps= mesService.getListEpps();
		
		Iterator<MesVo> ite= listEpps.iterator();
		while(ite.hasNext()){
			logger.info(ite.next().toString());
		}
		
		return listEpps;
	}
	
	
};

