package com.unomic.dulink.svg.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SVGVo{
	int id;
	String name;
	int x;
	int y;
	int w;
	int h;
	float width;
	String ieX;
	String ieY;
	float height;
	String viewBox;
	String d;
	String transform;
	String pic;
	String status;
	String lastChartStatus;
	double spd_load;
	double feed_override;
	String alarm;
	String endDateTime;
	String startDateTime;
	Integer shopId;
	int adt_id;
	int m_id;
	int dvcId;
	int adapter_id;
	String operationTime;
	int fontSize;
};
