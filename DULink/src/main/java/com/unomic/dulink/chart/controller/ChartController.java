package com.unomic.dulink.chart.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.BarChartVo;
import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.domain.DataVo;
import com.unomic.dulink.chart.domain.TimeChartVo;
import com.unomic.dulink.chart.service.ChartService;
import com.unomic.dulink.common.domain.CommonFunction;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/chart")
@Controller
public class ChartController {

	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService;

	@RequestMapping(value="getStartTime")
	@ResponseBody
	public String getStartTime(){
		String str = "";
		try {
			str = chartService.getStartTime();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getJigList")
	@ResponseBody
	public String getJicList(ChartVo chartVo){
		String str = null;
		try {
			str = chartService.getJicList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="getAlarmList")
	@ResponseBody
	public String getAlarmList(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getAlarmList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="dailyChart")
	public String dailyChart(ChartVo chartVo, HttpServletRequest request){
		request.setAttribute("WC", chartVo.getWC());
		request.setAttribute("dvcName", chartVo.getName());
		request.setAttribute("sDate", chartVo.getSDate());
		request.setAttribute("eDate", chartVo.getEDate());
		
		return "/chart/wcGraph";
	};
	
	@RequestMapping(value="getJigList4Report")
	@ResponseBody
	public String getJigList4Report(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getJigList4Report(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getDvcList")
	@ResponseBody
	public String getDvcList(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getDvcList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getWcData")
	@ResponseBody
	public String getWcData(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getWcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getWcDataByDvc")
	@ResponseBody
	public String getWcDataByDvc(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getWcDataByDvc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getTableData")
	@ResponseBody
	public String getTableData(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getTableData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getWcList")
	@ResponseBody
	public String getWcList(ChartVo chartVo){
		String str = null;
		try {
			str = chartService.getWcList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="highChartTest")
	public String highChartTest(){
		return "chart/highChartTest";
	};
	
	@RequestMapping(value="getAlarmData")
	@ResponseBody
	public String getAlarmData(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getAlarmData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="performanceReport")
	public String performanceReport(){
		return "chart/performanceReport";
	};
	
	@RequestMapping(value="jigGraph")
	public String jigGraph(){
		return "chart/jigGraph";
	};
	
	@RequestMapping(value="wcGraph")
	public String wcGraph(){
		return "chart/wcGraph";
	};
	
	@RequestMapping(value="alarmReport")
	public String alarmReport(){
		return "chart/alarmReport";
	};
	
	@RequestMapping(value="DIMM")
	public String DIMM(){
		return "chart/DIMM";
	};
	
	@RequestMapping(value="dimf")
	public String dkimf(){
		return "chart/DIMF";
	};
	
	@RequestMapping(value="edit")
	public String edit(){
		return "chart/dashBoard_edit";
	};
	
	@RequestMapping(value="main")
	public String main(){
		return "chart/dashBoard";
	};

	@RequestMapping(value="main_slide")
	public String main_slide(){
		return "chart/dashBoard_slide";
	};
	
	@RequestMapping(value="main2_slide")
	public String main2_slide(){
		return "chart/dashBoard2_slide";
	};
	
	@RequestMapping(value="main3_slide")
	public String main3_slide(){
		return "chart/dashBoard3_slide";
	};
	
	@RequestMapping(value="main4_slide")
	public String main4_slide(){
		return "chart/dashBoard4_slide";
	};
	
	@RequestMapping(value="main_en")
	public String main_en(){
		return "chart/dashBoard_en";
	};
	
	@RequestMapping(value="reSizing")
	public String reSizing(){
		return "chart/dashBoard_resizing";
	};
	
	@RequestMapping(value="dashBoard")
	public String dashBoard(){
		return "chart/dashBoard_sample";
	};
	
	@RequestMapping(value="multiVision")
	public String multiVision(){
		return "chart/multiVision";
	};
	
	@RequestMapping(value="index")
	public String index(){
		return "chart/index";
	};
	
	@RequestMapping(value="IE")
	public String IE(){
		return "chart/dashBoard_IE";
	};
	
	@RequestMapping(value="main2")
	public String main2(){
		return "chart/dashBoard2";
	};
	
	@RequestMapping(value="main3")
	public String main3(){
		return "chart/dashBoard3";
	};
	
	@RequestMapping(value="main4")
	public String main4(){
		return "chart/dashBoard4";
	};
	
	@RequestMapping(value="mobile")
	public String mobile(){
		return "chart/mobile";
	};

	@RequestMapping(value="mobile2")
	public String mobile2(){
		return "chart/mobile2";
	};
	
	@RequestMapping(value="highChart1")
	public String highChart1(){
		return "chart/highChart1";
	};
	
	@RequestMapping(value="highChart2")
	public String highChart2(){
		return "chart/highChart2";
	}
	
	@RequestMapping(value="highChart3")
	public String highChart3(){
		return "chart/highChart3";
	};
	
	@RequestMapping(value="highChart4")
	public String highChart4(){
		return "chart/highChart4";
	};
	
	@RequestMapping(value="video")
	public String video(){
		return "test/video";
	}
	
	@RequestMapping(value="video2")
	public String video2(){
		return "test/video2";
	};
	
	@RequestMapping(value="getStatusData")
	@ResponseBody
	public String getStatusData(ChartVo chartVo){
		String result = "";
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			//chartVo.setDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
			result = chartService.getStatusData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//result = chartVo.getCallback() + "(" + result + ")";
		return result;
	};
	
	@RequestMapping(value="test")
	@ResponseBody
	public void test(){
		ChartVo chartVo = new ChartVo();
		chartVo.setDate("2015-05-09");
		try {
			chartService.test(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@RequestMapping(value="getCurrentDvcData")
	@ResponseBody
	public ChartVo getCurrentDvcData(ChartVo chartVo){
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			
			chartVo = chartService.getCurrentDvcData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return chartVo;
	};
	
	//@Scheduled(fixedDelay = 5000)
	public void addData(){
		try {
			chartService.addData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
	
	@RequestMapping(value="getDvcName")
	@ResponseBody
	public String getDvcName(ChartVo chartVo){
		String dvcName = null;
		try {
			dvcName = chartService.getDvcName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dvcName;
	};
	
	@RequestMapping(value="getAllDvcStatus")
	@ResponseBody
	public String getAllDvcStatus(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getAllDvcStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getAllDvcId")
	@ResponseBody
	public String getAllDvcId(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getAllDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	
	@RequestMapping(value="getBarChartDvcId")
	@ResponseBody
	public String getBarChartDvcId2(ChartVo chartVo){
		String result = null;
		try {
			result = chartService.getBarChartDvcId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	public String addZero(String str){
		String rtn = str;
		if(rtn.length()==1){
			rtn = "0" + str;
		};
		return rtn;
	};
	
	@RequestMapping(value="getTime")
	@ResponseBody
	public String getTime(){
		String rtn = "";

		Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
		//cal.add ( cal.DATE, 1); //2개월 전....
		
		String year = String.valueOf(cal.get ( cal.YEAR ));
		String month = String.valueOf(cal.get ( cal.MONTH )+1);
		String day = String.valueOf(cal.get ( cal.DATE ));
		
		String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
		String minute = String.valueOf(cal.get(cal.MINUTE));
		String second = String.valueOf(cal.get(cal.SECOND));
		rtn = year + ". " + month + ". " + day + ".-" + hour + ":" + minute + ":" + second;
		return rtn;
	};
	
	@RequestMapping(value="dashBoard_pad")
	public String pad(){
		return "chart/dashBoard_pad";
	};
	
	@RequestMapping(value="dashBoard_left")
	public String pad_left(){
		return "chart/dashBoard_left";
	};
	
	@RequestMapping(value="dashBoard_right")
	public String pad_right(){
		return "chart/dashBoard_right";
	};
	
	@RequestMapping(value="dashBoard_center")
	public String center(){
		return "chart/dashBoard_center";
	};
	
	@RequestMapping(value="polling1")
	@ResponseBody
	public String polling1() throws Exception{
		return chartService.polling1();
	};
	
	@RequestMapping(value="setVideo")
	@ResponseBody
	public String setVideo(String id) throws Exception{
		return chartService.setVideo(id);
	};
	
	@RequestMapping(value="initVideoPolling")
	@ResponseBody
	public void initVideoPolling() throws Exception{
		chartService.initVideoPolling();
	}
	
	@RequestMapping(value="initPiePolling")
	@ResponseBody
	public void initPiePolling() throws Exception{
		chartService.initPiePolling();
	};
	
	@RequestMapping(value="piePolling1")
	@ResponseBody
	public String piePolling1() throws Exception{
		return chartService.piePolling1();
	};
	
	@RequestMapping(value="piePolling2")
	@ResponseBody
	public String piePolling2() throws Exception{
		return chartService.piePolling2();
	};
	
	@RequestMapping(value="setChart1")
	@ResponseBody
	public String setChart1(String id) throws Exception{
		return chartService.setChart1(id);
	};
	
	@RequestMapping(value="setChart2")
	@ResponseBody
	public String setChart2(String id) throws Exception{
		return chartService.setChart2(id);
	};
	
	@RequestMapping(value="delVideoMachine")
	@ResponseBody
	public void delVideoMachine(String id) throws Exception{
		chartService.delVideoMachine(id);
	};
	
	@RequestMapping(value="delChartMachine")
	@ResponseBody
	public void delChartMachine(String id) throws Exception{
		chartService.delChartMachine(id);
	};
	
	@RequestMapping(value="getAdapterId")
	@ResponseBody
	public String getAdapterId(ChartVo chartVo){
		String result = null;
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(cal.get(cal.HOUR_OF_DAY));
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			chartVo.setDate(year + "-" + month + "-" + date);
			
			if(Integer.parseInt(hour)>=20){
				cal.add ( cal.DATE, 1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setDate(year + "-" + month + "-" + date);
			};
			
			//result = chartService.getAllDvcId(chartVo);
			result = chartService.getAdapterId(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="showDetailEvent")
	@ResponseBody
	public String showDetailEvent(ChartVo chartVo) throws Exception{
		return chartService.showDetailEvent(chartVo);
	};
	
	@RequestMapping(value="getDetailStatus")
	@ResponseBody
	public String getDetailStatus(ChartVo chartVo)throws Exception{
		String result = null;
		try {
			Calendar cal = Calendar.getInstance ( );//오늘 날짜를 기준으루..
			//cal.add ( cal.DATE, 1); //2개월 전....
			
			String year = String.valueOf(cal.get ( cal.YEAR ));
			String month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
			String date = addZero(String.valueOf(cal.get ( cal.DATE )));
			
			String hour = String.valueOf(chartVo.getStartDateTime());
			String minute = String.valueOf(cal.get(cal.MINUTE));
			String second = String.valueOf(cal.get(cal.SECOND));
			
			chartVo.setDate(year + "-" + month + "-" + date);
			chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + hour + ":00");
			chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + hour + ":59");
			
			if(Integer.parseInt(hour)>=20 && Integer.parseInt(hour)!=24){
				cal.add ( cal.DATE, -1);
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + hour + ":00");
				chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + hour + ":59");
			}else if(Integer.parseInt(hour)==24){
				year = String.valueOf(cal.get ( cal.YEAR ));
				month = addZero(String.valueOf(cal.get ( cal.MONTH )+1));
				date = addZero(String.valueOf(cal.get ( cal.DATE )));
				
				chartVo.setStartDateTime(year + "-" + month + "-" + date + " " + "00:00");
				chartVo.setEndDateTime(year + "-" + month + "-" + date + " " + "00:59");
			};
			
			System.out.println(chartVo);
			//result = chartService.getAllDvcId(chartVo);
			result = chartService.getDetailStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	
	// need param
	// -- deprecated. : inputVo.date = workdate
	// inputVo.dvcId = dvcId
	// inputVo.targetDateTime = yyyy-MM-dd HH:mm:ss
	@RequestMapping(value="getTimeChart")
	@ResponseBody
	public List<BarChartVo> getTimeChart(ChartVo chartVo){
		String workDate = CommonFunction.dateTime2WorkDate(chartVo.getTargetDateTime());
		chartVo.setDate(workDate);
		
		List<BarChartVo> chart = new ArrayList<BarChartVo>();
		List<TimeChartVo> listTime =  chartService.getTimeChartData(chartVo);
		
		//rtn Data is empty.
		if(listTime == null){
			return null;
		}
		
		Iterator<TimeChartVo> ite = listTime.iterator();
		while(ite.hasNext()){
			TimeChartVo srcVo = ite.next();
			BarChartVo tmpVo = new BarChartVo();
			List<DataVo> data = new ArrayList<DataVo>();
			DataVo tmpDataVo = new DataVo();
			tmpDataVo.setStartTime(srcVo.getStartTime());
			tmpDataVo.setEndTime(srcVo.getEndTime());
			tmpDataVo.setY(srcVo.getY());
			data.add(tmpDataVo);
			tmpVo.setData(data);
			tmpVo.setColor(srcVo.getColor());
			chart.add(tmpVo);
		}
		return chart;
	};
	

	@RequestMapping(value="getOtherChartsData")
	@ResponseBody
	public ChartVo getOtherChartsData(ChartVo chartVo) throws Exception{
		return chartService.getOtherChartsData(chartVo);
	};
	
	@RequestMapping(value="testTimeChart")
	@ResponseBody
	public List<ChartVo> testTimeChart(ChartVo chartVo) throws Exception{
		return chartService.testTimeChartData(chartVo);
	};

	@RequestMapping(value="singleChartStatus")
	public String singleChartStatus(){
		return "chart/singleChartStatus";
	};
};

