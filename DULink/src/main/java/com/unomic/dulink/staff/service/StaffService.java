package com.unomic.dulink.staff.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.unomic.dulink.staff.domain.StaffVo;


public interface StaffService {
	public Map setStaffRegId(StaffVo staffVo) throws Exception;
	public List<StaffVo> getListStaff(StaffVo staffVo) throws Exception;
	public StaffVo getStaff(StaffVo staffVo) throws Exception;
}
