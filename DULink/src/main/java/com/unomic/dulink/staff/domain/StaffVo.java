package com.unomic.dulink.staff.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class StaffVo{
	Integer staffId;
	Integer teamId;
	
	String staffName;
	String teamName;
	String position;
	String cellNum;
	String regId;
}
