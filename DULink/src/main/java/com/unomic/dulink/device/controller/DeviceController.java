package com.unomic.dulink.device.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.adapter.service.AdapterService;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.device.domain.NightChartVo;
import com.unomic.dulink.device.service.DeviceService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/device")
@Controller
public class DeviceController {

	private static final Logger logger = LoggerFactory.getLogger(DeviceController.class);
	
	
	@Autowired
	private DeviceService deviceService;

	@Autowired
	private AdapterService adapterService;

	@RequestMapping(value="adjustDeviceStatus")
	public void adjustDeviceStatus(){
		deviceService.adjustDeviceStatus();
	}
	
	@RequestMapping(value="calcDeviceOptime")
	public void calcDeviceOptime(){
		DeviceVo setVo = new DeviceVo();
		setVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		deviceService.calcDeviceOptime(setVo);
	}
	
	@RequestMapping(value="calcDeviceTimes")
	@ResponseBody
	public String calcDeviceTimes(){
		DeviceVo setVo = new DeviceVo();
		setVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		deviceService.calcDeviceTimes(setVo);
		
		return "OK";
	}
	
	/*
	 * 현재 가동 상태를 저장하는 함수.
	 * 이후에 scheduled cron으로 아침 7시 마다 실행 시킬 예정.
	 */
	@RequestMapping(value="calcDailyMachineStatus")
	@ResponseBody
	public String calcDailyMachineStatus(){
		DeviceVo setVo = new DeviceVo();
		
		deviceService.calcDeviceTimes(setVo);
		
		return "OK";
	}
	
	
	/* 야간 무인 장비 가동시간을 가져오는 
	 * 
	 * 
	 * @author cannon
	 * 
	 * @param String targetDate 추출을 원하는 날짜. yyyy-MM-dd
	 * 
	 */
	@RequestMapping(value="getNightlyMachineStatus")
	@ResponseBody
	public List<NightChartVo> calcDailyMachineStatus(DeviceVo setVo){
		
		List<NightChartVo> rtnLint = deviceService.getNightlyDvcStatus(setVo);
		
		return rtnLint;
	}
	
	@RequestMapping(value="getNightlyMachineStatus_8")
	@ResponseBody
	public List<NightChartVo> calcDailyMachineStatus_8(DeviceVo setVo){
		
		List<NightChartVo> rtnLint = deviceService.getNightlyDvcStatus_8(setVo);
		
		return rtnLint;
	}
	
	@RequestMapping(value="testSp")
	@ResponseBody
	public String testSp(){
		DeviceVo setVo = new DeviceVo();
		setVo.setDvcId("1");
		setVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		return deviceService.testProcedure(setVo);
	}
	
	//Param workDate : yyyy-MM-dd
	//
	@RequestMapping(value="calcOpPf")
	@ResponseBody
	public String calcOpPf(DeviceVo inputVo){

		if(inputVo.getWorkDate()==null ){
			inputVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		}
		deviceService.calcDvcOpPf(inputVo);
		return "OK";
	}
	
	@RequestMapping(value="calcNight")
	@ResponseBody
	public String calcNight(DeviceVo inputVo){

		if(inputVo.getTargetDate()==null ){
			inputVo.setTargetDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
			
		}
		deviceService.calcNight(inputVo);
		return "OK";
	}
	
	@RequestMapping(value="calcNight_8")
	@ResponseBody
	public String calcNight_8(DeviceVo inputVo){

		if(inputVo.getTargetDate()==null ){
			inputVo.setTargetDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		}
		deviceService.calcNight_8(inputVo);
		return "OK";
	}
	
	
	
	//param1 [dvcId]
	//param2 [isNigh]t 1 = true, 0 = false
	@RequestMapping(value="setNight")
	@ResponseBody
	public String setNight(DeviceVo inputVo){


		deviceService.editSetNightDvc(inputVo);
		return "OK";
	}
	
	@RequestMapping(value="setSlide")
	@ResponseBody
	public String setSlide(DeviceVo inputVo){

		deviceService.editSetSlideSeqDvc(inputVo);
		return "OK";
	}
	
	
	//Param stDate : yyyy-MM-dd
	//Param edDate : yyyy-MM-dd
	@RequestMapping(value="batchOpPf")
	@ResponseBody
	public String batchOpPf(DeviceVo inputVo){
		if(inputVo.getStDate()==null || inputVo.getEdDate()==null){
			return "fail-check param";
		}
		logger.info("inputVo.getStDate():"+inputVo.getStDate());
		logger.info("inputVo.getEdDate():"+inputVo.getEdDate());
	       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
           Date dateSt = null;
           Date dateEd = null;
           try {
                  dateSt = dateFormat.parse(inputVo.getStDate());
                  dateEd = dateFormat.parse(inputVo.getEdDate());
                  
           } catch (ParseException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }
           Calendar cal = Calendar.getInstance();

           cal.setTime(dateSt);
           String tgDate = dateFormat.format(cal.getTime());
           inputVo.setWorkDate(tgDate);
           
           logger.info("tgDate:"+tgDate);
           
           while(!tgDate.equals(inputVo.getEdDate())){
        	   logger.info("tgDate:"+tgDate);
        	   deviceService.calcDvcOpPf(inputVo);
        	   
        	   cal.add(Calendar.DATE, 1);
               tgDate = dateFormat.format(cal.getTime());
               inputVo.setWorkDate(tgDate);
           }
		
		return "OK";
	}
	
	@RequestMapping(value="batchNight")
	@ResponseBody
	public String batchNight(DeviceVo inputVo){
		if(inputVo.getStDate()==null || inputVo.getEdDate()==null){
			return "fail-check param";
		}
		logger.info("inputVo.getStDate():"+inputVo.getStDate());
		logger.info("inputVo.getEdDate():"+inputVo.getEdDate());
		String edDate = inputVo.getEdDate();
	       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
           Date dateSt = null;
           Date dateEd = null;
           try {
                  dateSt = dateFormat.parse(inputVo.getStDate());
                  dateEd = dateFormat.parse(inputVo.getEdDate());
                  
           } catch (ParseException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }
           Calendar cal = Calendar.getInstance();

           cal.setTime(dateSt);
           String tgDate = dateFormat.format(cal.getTime());

           inputVo.setTargetDate(tgDate);
           while(!tgDate.equals(edDate)){
        	   deviceService.calcNight(inputVo);
        	   cal.add(Calendar.DATE, 1);
               tgDate = dateFormat.format(cal.getTime());
               inputVo.setTargetDate(tgDate);
           }
 
		return "OK";
	}
	
	@RequestMapping(value="batchNight_8")
	@ResponseBody
	public String batchNight_8(DeviceVo inputVo){
		if(inputVo.getStDate()==null || inputVo.getEdDate()==null){
			return "fail-check param";
		}
		logger.info("inputVo.getStDate():"+inputVo.getStDate());
		logger.info("inputVo.getEdDate():"+inputVo.getEdDate());
		String edDate = inputVo.getEdDate();
	       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
           Date dateSt = null;
           Date dateEd = null;
           try {
                  dateSt = dateFormat.parse(inputVo.getStDate());
                  dateEd = dateFormat.parse(inputVo.getEdDate());
                  
           } catch (ParseException e) {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
           }
           Calendar cal = Calendar.getInstance();

           cal.setTime(dateSt);
           String tgDate = dateFormat.format(cal.getTime());

           inputVo.setTargetDate(tgDate);
           while(!tgDate.equals(edDate)){
        	   deviceService.calcNight_8(inputVo);
        	   cal.add(Calendar.DATE, 1);
               tgDate = dateFormat.format(cal.getTime());
               inputVo.setTargetDate(tgDate);
           }
 
		return "OK";
	}
	
}

