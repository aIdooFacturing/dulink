package com.unomic.dulink.device.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.device.domain.NightChartVo;

public interface DeviceService {
	
	public String editLastStatus(DeviceVo inputVo);
	public String editLastStatusIOL(DeviceVo inputVo);
	public String adjustDeviceStatus();
	public String calcDeviceOptime(DeviceVo dvcVo);
	public String calcDeviceTimes(DeviceVo dvcVo);
	public String editDvcHealth(DeviceVo dvcVo);
	public String testProcedure(DeviceVo dvcVo);
	public String addDailyDvcStatus();
	public List<NightChartVo> getNightlyDvcStatus(DeviceVo inputVo);
	public List<NightChartVo> getNightlyDvcStatus_8(DeviceVo inputVo);
	public String calcDvcOpPf(DeviceVo inputVo);
	public String calcNight(DeviceVo inputVo);
	public String calcNight_8(DeviceVo inputVo);
	public String calcAWListEndTime();
	public String editSetNightDvc(DeviceVo inputVo);
	public String editSetSlideSeqDvc(DeviceVo inputVo);
	public String editMap();
};
