package com.unomic.dulink.device.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class AlarmVo{
	String adtId;
	String dvcId;
	String alarmStartTime;
	String alarmEndTime;
	String alarmHoldingTimeSec;
	String alarmCode;
	String alarmMsg;
}
