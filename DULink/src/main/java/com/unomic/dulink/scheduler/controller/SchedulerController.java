package com.unomic.dulink.scheduler.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.adapter.service.AdapterService;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.device.service.DeviceService;
import com.unomic.dulink.sensor.service.SensorService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/scheduler")
@Controller
public class SchedulerController {

	private static final Logger logger = LoggerFactory.getLogger(SchedulerController.class);
	
	@Autowired
	private DeviceService deviceService;

	@Autowired
	private AdapterService adapterService;

	@Autowired
	SensorService sensorService;
	

	@Scheduled(fixedDelay = 180000)
	public void samplingData(){

		DeviceVo inputVo = new DeviceVo();
		inputVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		calcOpPf(inputVo);
		inputVo.setTargetDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		calcNight(inputVo);
		
		calcAWListEndTime();
	}
	
	
	@Scheduled(cron="0 */30 * * * * ") 
	public void addTemperValue() throws Exception{
		sensorService.addListTemper(sensorService.parseListTemper());
	}

	@Scheduled(fixedDelay = 10000)
	public void runPolling(){
		getIOLdata();
	}
	
	@RequestMapping(value="testPolling")
	@ResponseBody
	public String testPolling(){

		getIOLdata();
		return "OK";
	}
	
	@RequestMapping(value="testSample")
	@ResponseBody
	public String testSample(){

		DeviceVo inputVo = new DeviceVo();
		inputVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		calcOpPf(inputVo);
		inputVo.setTargetDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		calcNight(inputVo);
		
		calcAWListEndTime();
		return "OK"+inputVo.getTargetDate();
	}

	@RequestMapping(value="testNight")
	@ResponseBody
	public String testNight(HttpSession session){
		DeviceVo inputVo = new DeviceVo();
		inputVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		calcNight(inputVo);
		
		return "OK";
	}
	@RequestMapping(value="testOpPf")
	@ResponseBody
	public String testOpPf(HttpSession session){
		DeviceVo inputVo = new DeviceVo();
		inputVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));;
		calcOpPf(inputVo);
		
		return "OK";
	}
	
	@RequestMapping(value="testGetTemper")
	@ResponseBody
	public String testGetTemper() throws Exception{
		sensorService.addListTemper(sensorService.parseListTemper());
		return "OK";
	}
	
	
	@RequestMapping(value="getVmType")
	@ResponseBody
	public String getVmType(){
		//由ъ뒪�듃 �븳踰덉뿉 update, insert �븯硫� �냽�룄 �뼢�긽 �뿬吏� �엳�쓬.
		String s = System.getProperty("java.vm.name");
		
		return s;
	}
	
	

	public void getIOLdata(){
		adapterService.getIOLData();
	}
	
	public void calcAWListEndTime(){
		deviceService.calcAWListEndTime();
	}
	
	public String calcDeviceTimes(){
		DeviceVo setVo = new DeviceVo();
		setVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
		deviceService.calcDeviceTimes(setVo);
		return "OK";
	}

	public void calcOpPf(DeviceVo inputVo){
		deviceService.calcDvcOpPf(inputVo);
	}
	
	public void calcNight(DeviceVo inputVo){
		deviceService.calcNight(inputVo);
		deviceService.calcNight_8(inputVo);
	}

}

