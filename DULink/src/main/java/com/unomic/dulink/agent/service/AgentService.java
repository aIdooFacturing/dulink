package com.unomic.dulink.agent.service;

import java.util.List;

import com.unomic.cnc.data.MemoryMap;
import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.smarti.nfc.domain.NFCVo;

public interface AgentService {
	
	public List<AdapterVo> getListAllStaff();
	
	public String addPureStatus(AdapterVo pureStatusVo);
	public String addListPureStatus(List<AdapterVo> listPureStatus);
	public String editLastEndTime(AdapterVo firstOfListVo);
	public String addIOLStatus(AdapterVo pureStatusVo);

};
