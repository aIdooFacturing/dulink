package com.unomic.dulink.agent.domain;

import java.util.List;

import com.unomic.smarti.nfc.domain.Alarm;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class AgentVo{
	
	String adtId;
	String dvcId;
	Long timeStamp;
	String mainProgName;
	String prgmHead;
	String mode;
	String status;
	String spdLoad;
	String feedOverride;
	String rapidFeedOverride;
	String runTimeMin;
	String runTimeMsec;
	String jsonAlarm;
	String jsonModal;
	String MD5;
	
	String localIp;
	String remoteIp;

}
