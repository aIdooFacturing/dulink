package com.unomic.dulink.agent.controller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.adapter.service.AdapterService;
import com.unomic.dulink.agent.domain.AgentVo;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value="/")
@Controller
public class CurrentController {

	private static final Logger logger = LoggerFactory.getLogger(CurrentController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	private HashMap<String, AdapterVo> mapDupleCheck = new HashMap<String, AdapterVo>();
	
	@Autowired
	private AdapterService adapterService;

	@RequestMapping(value = "/{dvcId}/current", method = RequestMethod.GET)
	@ResponseBody
    public String getDvcProbe(@PathVariable String dvcId, AgentVo agentVo) throws Exception{
		
		logger.info("current_dvcId:"+dvcId);
		//logger.info(agentVo.getDvcId());
        return "OK";
    }
	
	@RequestMapping(value = "/current", method = RequestMethod.GET)
	@ResponseBody
    public String getProbe(AgentVo agentVo) throws Exception{
		
		logger.info("Just current");
        return "OK";
    }
};

