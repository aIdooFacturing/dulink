package com.unomic.dulink.sensor.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.sensor.domain.SensorVo;
import com.unomic.dulink.sensor.domain.TemperVo;
import com.unomic.dulink.sensor.service.SensorService;

@RequestMapping(value = "/sensor")
@Controller
public class SensorController {

	private static final Logger logger = LoggerFactory.getLogger(SensorController.class);
	
	@Autowired
	SensorService sensorService;
	
	
	@RequestMapping(value="testParser")
	public void addTemperValue() throws Exception{
		
		sensorService.addListTemper(sensorService.parseListTemper());
		
	}
	
	@RequestMapping(value="getTemperListCSV")
	@ResponseBody
	public List<TemperVo> testGetList(SensorVo inputVo) throws Exception{
		
		List<TemperVo> rtnList = sensorService.getListTemperDate(inputVo);
		
		return rtnList;
	}
	
	@RequestMapping(value = "csv")
	public String csv(String csv) {
		try{
		}catch(Exception e){
			e.printStackTrace();
		}
		return "csv/csv1"; 
	}
	
	@RequestMapping(value="testDate")
	@ResponseBody
	public String testDate(SensorVo inputVo, ModelMap model){
	
		Map map = null;
		try {
			//map = adminService.getDscntList(inputVo);
			List<TemperVo> rtnList = sensorService.getListTemperDate(inputVo);
			model.put("listTemper", rtnList);
			return "OK";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		}
	}
	@RequestMapping(value="getTemperList")
	public String getTemperList(SensorVo inputVo, ModelMap model){
		try {
			List<TemperVo> rtnList = sensorService.getListTemperDate(inputVo);
			model.put("listTemper", rtnList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return "sensor/listTemper";
	};
	
	@RequestMapping(value="main")
	public String sensorPage(){
		
		return "sensor/listSensorInfo";
	}
		
};

