package com.unomic.dulink.sensor.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.device.domain.DeviceVo;
import com.unomic.dulink.device.domain.NightChartVo;
import com.unomic.dulink.sensor.domain.SensorVo;
import com.unomic.dulink.sensor.domain.TemperVo;

public interface SensorService {
	
	public List<TemperVo> parseListTemper() throws Exception;
	public String addListTemper(List<TemperVo> inputList);
	public List<TemperVo> getListTemperDate(SensorVo inputVo);
};
