package com.unomic.dulink.sensor.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class TemperVo{
	String temperId;
	String temperValue;
	String temperDatetime;
	String regdt;
}
