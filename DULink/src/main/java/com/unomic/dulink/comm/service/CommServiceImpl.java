package com.unomic.dulink.comm.service;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.comm.domain.CommVo;

@Service
@Repository
public class CommServiceImpl extends SqlSessionDaoSupport implements CommService {
	private static final String NAME_SPACE	="com.DULink.comm.";
	
	@Override
	@Transactional
	public String addMsgLog(CommVo commVo) throws Exception {
		// TODO Auto-generated method stub

		SqlSession sqlSession=getSqlSession();
		sqlSession.update(NAME_SPACE+"addMsgLog",commVo);
		
		return "OK";
	}
	

}
