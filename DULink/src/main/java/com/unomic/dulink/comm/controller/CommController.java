package com.unomic.dulink.comm.controller;


import gcm.Message;
import gcm.Result;
import gcm.Sender;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.comm.domain.CommVo;
import com.unomic.dulink.comm.service.CommService;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.staff.domain.StaffVo;
import com.unomic.dulink.staff.service.StaffService;

/**
 * Handles requests for the application home page.
 */

@Controller
@RequestMapping(value="/comm/") 
public class CommController {
	@Autowired
	private CommService commService;

	@Autowired
	private StaffService staffService;

	@Autowired
	private StaffService staffGroupService;
	
	private static final Logger logger = LoggerFactory.getLogger(CommController.class);
	private static final String PARAMETER_REG_ID = "regId";
	private static final String PARAMETER_MAC_ADDR = "mac";
	/**
	 * Simply selects the home view to render by returning its name.
	 */

	@RequestMapping("index")
	public String index(Model model) {
		logger.info("Welcome home! The client locale is {}.");
		//commService.test();

		return "regist/indexRegist";
	}

	@RequestMapping("sendMsg")
	@ResponseBody
	public String sendMsg(CommVo commVo){
		int cnt=0;
		String msg = "";
		ObjectMapper om = new ObjectMapper();
		TimeZone tz;
		Date today = new Date();
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss (z Z)");
		DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		tz = TimeZone.getTimeZone("Asia/Seoul");
		df.setTimeZone(tz);
		String thisTime=df.format(today);
		logger.info("msg:"+commVo.getMsg());
		logger.info("decoded msg:"+URLDecoder.decode(commVo.getMsg()), "utf-8");

		Map map = new HashMap();

		map.put("type","chat");
		map.put("msg",commVo.getMsg());
		map.put("from",commVo.getSendId());
		map.put("fromName",commVo.getSendName());
		map.put("to",commVo.getRcvId());
		map.put("time",thisTime);

		try {
			msg=om.defaultPrettyPrintingWriter().writeValueAsString(map);
		} catch (JsonGenerationException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (JsonMappingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		logger.info("json4gcm:"+msg);

		//return "test";

		Message message = new Message.Builder().addData("msg",msg).build();

		StaffVo rcvStaffVo = new StaffVo();
		StaffVo sndStaffVo = new StaffVo();
		//List <TalkGroupVo> listRecvStaff = new ArrayList<TalkGroupVo>();
		//List <TalkGroupVo> listSendStaff = new ArrayList<TalkGroupVo>();
		rcvStaffVo.setStaffId(commVo.getRcvId());
		sndStaffVo.setStaffId(commVo.getSendId());

		try {
			rcvStaffVo = staffService.getStaff(rcvStaffVo);
			sndStaffVo = staffService.getStaff(sndStaffVo);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			try {
				commVo.setResult(CommonCode.CODE_GCM_FAIL);
				//commService.addMsgLog(commVo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			e1.printStackTrace();
			return "no";
		}
		String gcmResult;
		boolean checker;

		checker=("OK"==sendGCM(sndStaffVo, message) && "OK"==sendGCM(rcvStaffVo, message));
		
		if(checker){
			try {
				logger.info("add msg success");
				commVo.setResult(CommonCode.CODE_GCM_SUCCESS);
				//commService.addMsgLog(commVo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "ok";
		}else{
			try {
				logger.info("add msg fail");
				commVo.setResult(CommonCode.CODE_GCM_FAIL);
				//commService.addMsgLog(commVo);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "fail";
		}
	}

	private String sendGCM(StaffVo staffVo, Message message)
	{
		Sender sender = new Sender(CommonCode.myApiKey);

		//TalkGroupVo rcvGroupVo;
		Result result = null;
		try {
			logger.info("regId:"+staffVo.getRegId());
			result = sender.send(message, staffVo.getRegId(), 5);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("======= Send ======:"+result);
		return "OK";
	}
	
}
