package com.unomic.dulink.comm.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CommVo {
	private String mac;
	
	private String regId;
	private String ipAddr;
	private String usrId;
	private String staffId;
	
	private Integer sendId;
	private String sendName;
	private Integer rcvId;
	
	private String name;
	private String msg;
	private String msgNo;
	private String result;
	
}
