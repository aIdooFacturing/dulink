package com.unomic.dulink.comm.service;

import com.unomic.dulink.comm.domain.CommVo;

public interface CommService {
	public String addMsgLog(CommVo commVo) throws Exception;
	
}
