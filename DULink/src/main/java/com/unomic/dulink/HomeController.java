package com.unomic.dulink;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonFunction;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/home")
@Controller
public class HomeController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate );
		return "home";
	}
	
	@RequestMapping(value="testMil2Workdate")
	@ResponseBody
	public String testMil2Workdate(){
		Long sysTime = System.currentTimeMillis();
		
		logger.error(""+CommonFunction.mil2WorkDate(sysTime));
		
		//return sysTime.toString();
		return "OK";
	}
	
	@RequestMapping(value="testDate2Workdate")
	@ResponseBody
	public String testDate2Workdate(){
		String testTime = "2015-05-11 19:00:00";
		
		logger.error(""+CommonFunction.dateTime2WorkDate(testTime));
		
		//return sysTime.toString();
		return "OK";
	}
}
