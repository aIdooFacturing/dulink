package com.unomic.dulink.mobile.service;

import java.util.List;

import com.unomic.cnc.data.MemoryMap;
import com.unomic.dulink.mobile.domain.MobileVo;
import com.unomic.smarti.nfc.domain.NFCVo;

public interface MobileService {

	public List<MobileVo> listMachine();

};
