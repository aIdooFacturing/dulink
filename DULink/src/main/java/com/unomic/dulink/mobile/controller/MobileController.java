package com.unomic.dulink.mobile.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.unomic.dulink.mobile.service.MobileService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/mobile")
@Controller
public class MobileController {

	private static final Logger logger = LoggerFactory.getLogger(MobileController.class);
	private final static String namespace= "com.unomic.dulink.mobile,";
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private MobileService mobileService;

	@RequestMapping(value="indexDvc")
	public String conn(HttpSession session, String ip, String port){

		return "mobile/indexDvc";
	}
	
	@RequestMapping(value="dvcChart")
	public String dvcChart(HttpSession session, String ip, String port){

		return "mobile/dvcChart";
	}
	
//	@RequestMapping(value = "listMachine")
//	public String listSecuLv(ModelMap model) {
//		try{
//			Map map= mobileService.getListMachine();
//			model.addAllObjects(map);
//			
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return "secu/listSecuLv";
//	}
	
};

