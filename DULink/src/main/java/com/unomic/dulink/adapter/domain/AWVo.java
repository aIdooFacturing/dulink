package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.CommonCode;



@Getter
@Setter
@ToString
public class AWVo{
	
	String adtId;
	Integer dvcId;
	String workDate;
	String chartStatus;
	String startDateTime;
	String endDateTime;
	Integer holdingTimeSec;
	String alarmCode;
	String alarmMsg;
	
}
