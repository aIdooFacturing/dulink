package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.CommonCode;



@Getter
@Setter
@ToString
public class AdapterVo{
	
	Long timeStamp;	
	String jsonAlarm;
	String jsonModal;
	String MD5;
	
	String dvcName;
	String adtIp;
	String adtname;
	
	String localIp;
	String remoteIp;

	String adtLog;
	String logTime;
	String adtStatus;
	String lastHealthTime;
	
	String adtId;
	Long startUnixSec;
	String startDateTime;
	String endDateTime;
	String mode;
	String status;
	String chartStatus;
	String mainPrgmName;
	String currentPrgmName;
	
	String prgmHead;
	String modal;

	Float spdLoad;
	Integer feedOverride;
	Integer rapidFeedOverride;
	
	Integer spdOverride;
	Integer actualFeed;
	Integer spdActualSpeed;
	
	Integer runTimeMin;
	Integer runTimeMsec;
	String alarm;
	String workDate;
	
	Boolean isZeroSpdLoad;
	Boolean isZeroFeedOverride;
	Boolean isZeroActualFeed;
	Boolean isZeroActualSpindle;
	
	Boolean ioPower;
	Boolean ioInCycle;
	Boolean ioWait;
	Boolean ioAlarm;
	String modal_g0;
	String modal_g1;
	String modal_g2;
	String modal_g3;
	
	String modal_m1;
	String modal_m2;
	String modal_m3;
	String modal_t;
	String regdt;
	
	String m1;
	String m2;
	String m3;
	
	public AdapterVo(){
		this.setIoPower(false);
    	this.setIoInCycle(false);
    	this.setIoAlarm(false);
    	this.setIoWait(false);
    	this.setAlarm("[{\"alarmMsg\":\"\",\"alarmCode\":\"\"},{\"alarmMsg\":\"\",\"alarmCode\":\"\"}]");
    	this.setMode("");
    	this.setIsZeroSpdLoad(false);
    	this.setIsZeroFeedOverride(false);
    	this.setIsZeroActualFeed(false);
    	this.setIsZeroActualSpindle(false);
    	
    	this.setPrgmHead("");
    	this.setMainPrgmName("");
    	this.setStartDateTime("2015-05-12 17:00:00");
    	this.setChartStatus(CommonCode.MSG_STAT_INIT);
    	
    	this.spdLoad = (float) 0;
    	this.feedOverride = 0;
    	this.rapidFeedOverride = 0;
    	this.spdOverride = 0;
    	this.actualFeed = 0;
    	this.spdActualSpeed = 0;
	}
}
