package com.unomic.dulink.adapter.domain;

import java.io.FileReader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class RogerParser 
{
	private InputSource is;
	private Document document;
	private XPath xpath;
	private String filename;
	
	public RogerParser(Document doc) {
		this.document = doc;
	}
	
	public void Init() {
		try { 
			xpath = XPathFactory.newInstance().newXPath();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// id로 구분해서 데이터를 넣는다.
	private void InputData(MTConnect con, String name, String data) {
		switch(name) {
		case "actual_feed" : 
			con.setActual_feed(data);
			break;
		case "spindle_actual_speed" :
			con.setSpindle_actual_speed(data);
			break;
		case "spindle_load" : 
			con.setSpindle_load(data);
			break;
		case "feed_override" : 
			con.setFeed_override(data);
			break;
		case "mode" : 
			con.setMode(data);
			break;
		case "nc_status" : 
			con.setNc_status(data);
			break;
		case "rapid_override" :
			con.setRapid_override(data);
			break;
		case "spindle_override" :
			con.setSpindle_override(data);
			break;
		case "modal" :
			con.setModal(data);
			break;
		case "program" :
			con.setProgram(data);
			break;
		case"program_header" :
			con.setProgram_header(data);
			break;
		case"alarm1" :
			con.setAlarm1(data);
			break;
		case"alarm2" :
			con.setAlarm2(data);
			break;
		default:
			break;
		}
	}
	
	// 파싱 실행
	public void execute(MTConnect con) {
		try {
			NodeList cols = (NodeList)xpath.compile("//MTConnectStreams//Header").evaluate(document, XPathConstants.NODESET);
					
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				con.setCreationTime(cols.item(i).getAttributes().getNamedItem("creationTime").getTextContent()); 
			}
			
			cols = (NodeList)xpath.compile("//MTConnectStreams/Streams/DeviceStream").evaluate(document, XPathConstants.NODESET);
						
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				con.setName(cols.item(i).getAttributes().getNamedItem("name").getTextContent()); 
				con.setUuid(cols.item(i).getAttributes().getNamedItem("uuid").getTextContent());
			}
			
			cols = (NodeList)xpath.compile("//Samples/*").evaluate(document, XPathConstants.NODESET);
			
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				NamedNodeMap map = cols.item(i).getAttributes();
							
				for(int j = 0; j < map.getLength(); j++) {
					getDataXML(map.item(j), cols.item(i), "actual_feed", con);
					getDataXML(map.item(j), cols.item(i), "spindle_actual_speed", con);
					getDataXML(map.item(j), cols.item(i), "spindle_load", con);
				}
			}
			
			cols = (NodeList)xpath.compile("//Condition/*").evaluate(document, XPathConstants.NODESET);
			
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				NamedNodeMap map = cols.item(i).getAttributes();
							
				for(int j = 0; j < map.getLength(); j++) {
					getDataXML(map.item(j), cols.item(i), "alarm1", con);
					getDataXML(map.item(j), cols.item(i), "alarm2", con);
				}
			}
			
			cols = (NodeList)xpath.compile("//Events/*").evaluate(document, XPathConstants.NODESET);
			
			for(int i = 0 ; i < cols.getLength() ; i++ ) {
				NamedNodeMap map = cols.item(i).getAttributes();
							
				for(int j = 0; j < map.getLength(); j++) {
					getDataXML(map.item(j), cols.item(i), "feed_override", con);
					getDataXML(map.item(j), cols.item(i), "mode", con);
					getDataXML(map.item(j), cols.item(i), "nc_status", con);
					getDataXML(map.item(j), cols.item(i), "rapid_override", con);
					getDataXML(map.item(j), cols.item(i), "spindle_override", con);
					getDataXML(map.item(j), cols.item(i), "modal", con);
					getDataXML(map.item(j), cols.item(i), "program", con);
					getDataXML(map.item(j), cols.item(i), "program_header", con);
				}
			}
			
			cols = (NodeList)xpath.compile("//Events/*").evaluate(document, XPathConstants.NODESET);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void getDataXML(Node map, Node col, String name, MTConnect con) {
		if(map.getNodeName().equals("dataItemId") && map.getNodeValue().equals(name)) {
			InputData(con, name, col.getTextContent());
		}
	}

}