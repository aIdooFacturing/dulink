package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.CommonCode;



@Getter
@Setter
@ToString
public class McPrgmVo{
	
	String prgmId;
	Integer dvcId;
	Integer adtId;
	String prgmName;
	String prgmType;
	String mcType;
	Integer avgTimeSec;
	Integer cntSample;
		
}
