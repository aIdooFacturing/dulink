package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MTConnect {
	private String name;
	private String creationTime;
	private String uuid;
	private String actual_feed;
	private String spindle_actual_speed;
	private String spindle_load;
	private String feed_override;
	private String mode;
	private String nc_status;
	private String rapid_override;
	private String spindle_override;
	private String modal;
	private String program;
	private String program_header;
	private String alarm1;
	private String alarm2;
	
}