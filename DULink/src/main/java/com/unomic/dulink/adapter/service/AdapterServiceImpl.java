package com.unomic.dulink.adapter.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.ibatis.session.SqlSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;

import com.unomic.dulink.adapter.domain.AWVo;
import com.unomic.dulink.adapter.domain.AdapterVo;
import com.unomic.dulink.adapter.domain.MTConnect;
import com.unomic.dulink.adapter.domain.McPrgmCheckerVo;
import com.unomic.dulink.adapter.domain.McPrgmVo;
import com.unomic.dulink.adapter.domain.RogerParser;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.device.domain.DeviceVo;


@Service
@Repository
public class AdapterServiceImpl extends SqlSessionDaoSupport implements AdapterService{
	private final static String ADAPTER_SPACE= "com.dulink.adapter.";
	private final static String DEVICE_SPACE= "com.dulink.device.";
	
	@Override
	public List<AdapterVo> getListAllStaff(){
		// TODO Auto-generated method stub
		SqlSession sqlSession=getSqlSession();
		List<AdapterVo> rtnList = sqlSession.selectList(ADAPTER_SPACE+"getListAllAdapter");
		return rtnList;
	}
	
	@Override
	public List<McPrgmVo> getListMcPrgm(){
		// TODO Auto-generated method stub
		SqlSession sqlSession=getSqlSession();
		List<McPrgmVo> rtnList = sqlSession.selectList(ADAPTER_SPACE+"getListMcPrgm");
		return rtnList;
	}
	
	//Using sDate 'yyyy-mm-dd'
	//Using eDate 'yyyy-mm-dd'
	@Override
	public List<McPrgmCheckerVo> getListAPCDvc(McPrgmCheckerVo inputVo){
	
		// TODO Auto-generated method stub
		logger.info("inputVo:"+inputVo);
		SqlSession sqlSession=getSqlSession();
		List<McPrgmCheckerVo> rtnList = sqlSession.selectList(ADAPTER_SPACE+"getListAPCDvc",inputVo );
		return rtnList;
	}
	
	@Override
	@Transactional
	public String addPureStatus(AdapterVo pureStatusVo)
	{
		SqlSession sqlSession=getSqlSession();
		logger.info("addPureStatus:"+pureStatusVo);
		sqlSession.insert(ADAPTER_SPACE+"addPureData", pureStatusVo);
		return "OK";
	}
	
	@Override
	public List<AdapterVo> getListIolIp(){
		SqlSession sql=getSqlSession();
		List<AdapterVo> rtnList = sql.selectList(ADAPTER_SPACE+"listIOLIp");
		
		return rtnList;
	}
	@Override
	@Transactional
	public String addIOLStatus(AdapterVo pureStatusVo)
	{
		SqlSession sqlSession=getSqlSession();
		sqlSession.insert(ADAPTER_SPACE+"addIOLData", pureStatusVo);
		return "OK";
	}
	
	@Override
	@Transactional
	public String addListPureStatus(List<AdapterVo> listPureStatus)
	{
		logger.info("addListPureStatus");
		SqlSession sqlSession=getSqlSession();
		
		AdapterVo firstAdapterVo = listPureStatus.get(0);
		//AdapterVo tmpAdapterVo = (AdapterVo) sqlSession.selectOne(ADAPTER_SPACE + "getLastStartTime", firstAdapterVo);
		//firstAdapterVo.setEndDateTime(tmpAdapterVo.getStartDateTime());
		
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		int listSize = listPureStatus.size();
		
		if(listSize<1){
			return "OK";
		}
		//logger.info("addListSize:"+listSize);
		
		dataMap.put("listPureStatus", listPureStatus);
		dataMap.put("adtId", firstAdapterVo.getAdtId());
		
		sqlSession.delete(ADAPTER_SPACE + "rmvDuplePureData", dataMap);
		sqlSession.insert(ADAPTER_SPACE + "addListPureData", dataMap);
		
//		if(listSize<100){
//			dataMap.put("listPureStatus", listPureStatus);
//			sqlSession.insert(ADAPTER_SPACE + "addListPureData", dataMap);
//		}else{
//			logger.info("@@@@@add List Big Size:"+listPureStatus.size());
//			List<AdapterVo> listPureStatus1 = listPureStatus.subList(0, listPureStatus.size()/2);
//			List<AdapterVo> listPureStatus2 = listPureStatus.subList(listPureStatus.size()/2, listPureStatus.size());
//			
//			logger.info("@@@@@add listPureStatus1:" + listPureStatus1.size());
//			logger.info("@@@@@add listPureStatus2:" + listPureStatus2.size());
//			dataMap.put("listPureStatus", listPureStatus1);
//			dataMap2.put("listPureStatus", listPureStatus2);
//			sqlSession.insert(ADAPTER_SPACE + "addListPureData", dataMap);
//			sqlSession.insert(ADAPTER_SPACE + "addListPureData", dataMap2);
//		}

		return "OK";
	}
	
	@Override
	@Transactional
	public String editLastEndTime(AdapterVo firstOfListVo)
	{
		logger.info("editLastEndTime");
		SqlSession sql=getSqlSession();

		int cnt = (int) sql.selectOne(ADAPTER_SPACE + "cntAdapterStatus", firstOfListVo);
		if(cnt < 1){
			return "OK";
		}
		
		AdapterVo tmpAdapterVo = (AdapterVo) sql.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
		if(tmpAdapterVo != null){
			sql.delete(ADAPTER_SPACE + "removeExceptionEndTime",tmpAdapterVo);
		}
		tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());
		sql.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
		
		return "OK";
	}
	
	@Override
	@Transactional
	public String editAdapterHealth(AdapterVo adapterVo)
	{
		SqlSession sql=getSqlSession();
		sql.update(ADAPTER_SPACE +"editAdapterHealth", adapterVo);

		return "OK";
	}
	
	@Override
	public List<AdapterVo> getListWinAgent()
	{
		SqlSession sql=getSqlSession();		
		List<AdapterVo> rtnList  = sql.selectList(ADAPTER_SPACE + "getListWinAgent");
		return rtnList;
	}

	@Override
	public List<AdapterVo> getListAdapterHealth()
	{
		SqlSession sql=getSqlSession();
		List<AdapterVo> listAdapter = sql.selectList(ADAPTER_SPACE +"listLastHealth");

		return listAdapter;
	}
	
	
	
	@Override
	@Transactional
	public String addAdtLog(AdapterVo adtVo)
	{
		SqlSession sql=getSqlSession();
		
		sql.insert(ADAPTER_SPACE + "addAdtLog", adtVo);

		return "OK";
	}
	
	@Override
	public List<AdapterVo> getListIOL()
	{
		SqlSession sql=getSqlSession();		
		List<AdapterVo> rtnList  = sql.selectList(ADAPTER_SPACE + "getListIOL");
		return rtnList;
	}
	
	@Override
	public AdapterVo chkAdtStatus(AdapterVo adtVo)
	{
		SqlSession sql = getSqlSession();
		
		AdapterVo rtnVo  = (AdapterVo) sql.selectOne(ADAPTER_SPACE + "chkAdtStatus",adtVo);
		
		return rtnVo;
	}
	
	@Override
	public int cntAdtStatus(AdapterVo adtVo)
	{
		SqlSession sql = getSqlSession();
		Integer cnt =  (Integer) sql.selectOne(ADAPTER_SPACE + "cntAdapterStatus",adtVo);
		return cnt;
	}
	
	@Override
	@Transactional
	public String addPureData(DeviceVo dvcVo, List<AdapterVo> listAdt)
	{
		long startTime = System.currentTimeMillis();
		editDvcHealth(dvcVo);
		AdapterVo preVo = new AdapterVo();
		preVo.setAdtId(dvcVo.getAdtId());
		
		//Need is zero test
		
		preVo = getLastInputData(preVo);
		logger.info("After getLastInputData");
		preVo = setAdtStatus(preVo);
		logger.info("After setAdtStatus");
		listAdt = chkDateStarterADT(preVo, listAdt);
		logger.info("After chkDateStarterADT");
		listAdt = checkListDuple(preVo, listAdt);
		logger.info("After checkListDuple");
		listAdt = setListChartStatus(listAdt);
		logger.info("After setListChartStatus");
		listAdt = removeListStatusDuple(preVo, listAdt);
		logger.info("After removeListStatusDuple");
		
		if(null==listAdt || listAdt.size() < 1){
			//remove Duple data.
			//isn't it need set last data?
			
			return "OK";
		}
		
		int dupleListSize = listAdt.size();
		
		logger.error("dupleListSize:"+dupleListSize);
		List<AdapterVo> listAdapterVo = setListEndTime(listAdt);

		DeviceVo inputVo = new DeviceVo();
		AdapterVo tmpVo = listAdapterVo.get(dupleListSize-1); 
		
		inputVo.setAdtId(tmpVo.getAdtId());
		inputVo.setLastUpdateTime(tmpVo.getStartDateTime());
		inputVo.setLastAlarm(tmpVo.getAlarm());
		inputVo.setLastFeedOverride(tmpVo.getFeedOverride());
		inputVo.setLastSpdLoad(tmpVo.getSpdLoad());
		inputVo.setLastChartStatus(tmpVo.getChartStatus());
		inputVo.setLastModal(tmpVo.getModal());
		inputVo.setLastStartDateTime(tmpVo.getStartDateTime());
		if(tmpVo.getPrgmHead().length()>20){
			inputVo.setLastProgramHeader(tmpVo.getPrgmHead().substring(0, 20));
		}else{
			inputVo.setLastProgramHeader(tmpVo.getPrgmHead());
		}
		if(tmpVo.getMainPrgmName().length()>5){
			inputVo.setLastProgramName(tmpVo.getMainPrgmName().substring(0, 5));
		}else{
			inputVo.setLastProgramName(tmpVo.getMainPrgmName());
		}
		long endTime = System.currentTimeMillis();
		long lTime = endTime - startTime;
		logger.info("TIME0 : " + lTime + "(ms)");

		startTime = System.currentTimeMillis();
		
		editLastDvcStatus(inputVo);
		
		endTime = System.currentTimeMillis();
		lTime = endTime - startTime;
		logger.info("TIME1 : " + lTime + "(ms)");

		startTime = System.currentTimeMillis();
		editLastEndTime(listAdapterVo.get(0));
		endTime = System.currentTimeMillis();
		lTime = endTime - startTime;
		logger.info("TIME2 : " + lTime + "(ms)");
		
		startTime = System.currentTimeMillis();
		addListPureStatus(listAdapterVo);
		endTime = System.currentTimeMillis();
		lTime = endTime - startTime;
		logger.info("TIME3 : " + lTime + "(ms)");

		startTime = System.currentTimeMillis();
		addListAWStatus(listAdapterVo);
		endTime = System.currentTimeMillis();
		lTime = endTime - startTime;
		logger.info("TIME4 : " + lTime + "(ms)");
		
		return "OK";
	}
	
	public AdapterVo getLastInputData(AdapterVo inputVo){
		AdapterVo newVo = new AdapterVo();
		newVo.setAdtId(inputVo.getAdtId());
		
		SqlSession sql = getSqlSession();
		Integer cnt =  (Integer) sql.selectOne(ADAPTER_SPACE + "cntAdapterStatus",inputVo);
		
		if(1 < cnt){
			newVo  = (AdapterVo) sql.selectOne(ADAPTER_SPACE + "getLastAdapterStatus",inputVo);
		}
    	return newVo;
	}
	
	private List<AdapterVo> chkDateStarterADT(AdapterVo preVo, List<AdapterVo> list){
		for(int i=0;i<list.size();i++){
			AdapterVo tmpVo = list.get(i);

			if(CommonFunction.isDateStart(preVo.getStartDateTime(), tmpVo.getStartDateTime())){

				AdapterVo starterVo = new AdapterVo();
				starterVo.setAdtId(tmpVo.getAdtId());
				starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
				starterVo.setEndDateTime(CommonFunction.getStandardP1SecToday());
				starterVo.setChartStatus(CommonCode.MSG_DATE_START);
				starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
				
				tmpVo.setStartDateTime(CommonFunction.getStandardP1SecToday());
				list.add(i, starterVo);
				list.set(i+1, tmpVo);
				
				break;
			}
		}
		return list;
	}
	
	private List<AdapterVo> checkListDuple(AdapterVo preVo,List<AdapterVo> listInput){
		logger.info("checkListDuple");
		List<AdapterVo> listOutput = new ArrayList<AdapterVo>();
		int size = listInput.size();
		AdapterVo innerPreVo = new AdapterVo();
		innerPreVo = preVo;
		for(int i=0;i<size;i++){
			AdapterVo crtVo = isDuple( innerPreVo, listInput.get(i));

			if(null == crtVo){
				continue;
			}else{
				innerPreVo = crtVo;
				listOutput.add(crtVo);
			}
		}
		return listOutput;
	}
	
	private AdapterVo isDuple(AdapterVo preVo, AdapterVo inputVo){
		//logger.info("isDuple");
		Long preStartTime = 0L;
		if(null == preVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		
		if(crtStartTime<preStartTime){
			logger.error("timeTrash data");
			return null;
		}
		//logger.info("before duple check");
		if (preVo.getCurrentPrgmName() == null || preVo.getCurrentPrgmName().length() == 0) {
		    // 媛믪씠 �뾾�뒗 寃쎌슦 泥섎━
		    preVo.setCurrentPrgmName("e1");
		} 
		if (inputVo.getCurrentPrgmName() == null || inputVo.getCurrentPrgmName().length() == 0) {
			inputVo.setCurrentPrgmName("e2");
		} 
		
		
		if(preVo.getAlarm().equals(inputVo.getAlarm())
			&&preVo.getFeedOverride().equals(inputVo.getFeedOverride())
			&&preVo.getMode().equals(inputVo.getMode())
			&&preVo.getIsZeroSpdLoad().equals(inputVo.getIsZeroSpdLoad())
			&&preVo.getIsZeroFeedOverride().equals(inputVo.getIsZeroFeedOverride())
			//&&preVo.getIsZeroActualFeed().equals(inputVo.getIsZeroActualFeed())
			&&preVo.getPrgmHead().equals(inputVo.getPrgmHead())
			&&preVo.getMainPrgmName().equals(inputVo.getMainPrgmName())
			&&preVo.getModal_m1().equals(inputVo.getModal_m1())
			//&&preVo.getModal_m2().equals(inputVo.getModal_m2())
			//&&preVo.getModal_m3().equals(inputVo.getModal_m3())
			&&preVo.getCurrentPrgmName().equals(inputVo.getCurrentPrgmName()) 
		)
		{
			//logger.info("Duple");
	    	return null;
		}else{

    		return inputVo;
		}
	}
	
	private List<AdapterVo> setListChartStatus(List<AdapterVo> listInput){
		int size = listInput.size();
		
		List <McPrgmVo> listAPCPrgm = getListMcPrgm();
	
		for(int i=0 ; i<size ; i++){
			AdapterVo tmpVo = listInput.get(i);
			int cntAPC = getCntAPCPrgm(listAPCPrgm, tmpVo);
			//logger.info("cntAPC:"+cntAPC);
			
			if(null != tmpVo.getChartStatus() && tmpVo.getChartStatus().equals(CommonCode.MSG_DATE_START)){
				continue;
			}else if(tmpVo.getStatus().charAt(2)=='1'){
				tmpVo.setChartStatus(CommonCode.MSG_ALARM);
			}else if(tmpVo.getStatus().charAt(3)=='1'){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if(tmpVo.getStatus().charAt(1)=='1'){
				tmpVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
			}else if(tmpVo.getStatus().charAt(1)=='0' && tmpVo.getStatus().charAt(2)=='0' && tmpVo.getStatus().charAt(3)=='0'){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if( !tmpVo.getAlarm().equals(CommonCode.MSG_NO_ALARM )
					
//					&& tmpVo.getIsZeroFeedOverride()
					&& tmpVo.getIsZeroSpdLoad()
					&& tmpVo.getIsZeroActualSpindle()
//					&& ((tmpVo.getIsZeroActualFeed())
//						|| ((tmpVo.getModal_m1().equals("5")||tmpVo.getModal_m2().equals("5")||tmpVo.getModal_m3().equals("5"))
//						|| (tmpVo.getModal_m1().equals("05")||tmpVo.getModal_m2().equals("05")||tmpVo.getModal_m3().equals("05"))))
					&& !tmpVo.getAlarm().equals(CommonCode.MSG_EXCEPTION_ALARM )
					)
			{
				tmpVo.setChartStatus(CommonCode.MSG_ALARM);
			}else if( cntAPC > 0 ){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if(! ( tmpVo.getMode().equals("MEM") || tmpVo.getMode().equals("MEMORY")) ){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			/*}else if(tmpVo.getModal_m1().equals("00")||tmpVo.getModal_m2().equals("00")||tmpVo.getModal_m3().equals("00")||
						tmpVo.getModal_m1().equals("0")||tmpVo.getModal_m2().equals("0")||tmpVo.getModal_m3().equals("0")){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if(tmpVo.getModal_m1().equals("01")||tmpVo.getModal_m2().equals("01")||tmpVo.getModal_m3().equals("01")||
					tmpVo.getModal_m1().equals("1")||tmpVo.getModal_m2().equals("1")||tmpVo.getModal_m3().equals("1")){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if(tmpVo.getModal_m1().equals("60")||tmpVo.getModal_m2().equals("60")||tmpVo.getModal_m3().equals("60")){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if(tmpVo.getIsZeroSpdLoad() && (tmpVo.getModal_m1().equals("30")||tmpVo.getModal_m1().equals("60"))){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);*/
			}else if(tmpVo.getModal_m1().equals("00")|| tmpVo.getModal_m1().equals("0")){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if(tmpVo.getModal_m1().equals("01")||tmpVo.getModal_m2().equals("01")||tmpVo.getModal_m3().equals("01")||
					tmpVo.getModal_m1().equals("1")||tmpVo.getModal_m2().equals("1")||tmpVo.getModal_m3().equals("1")){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if(tmpVo.getModal_m1().equals("60")||tmpVo.getModal_m2().equals("60")||tmpVo.getModal_m3().equals("60")){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if(tmpVo.getIsZeroSpdLoad() && (tmpVo.getModal_m1().equals("30")||tmpVo.getModal_m1().equals("60"))){
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}else if( "STRT".equals(tmpVo.getStatus()) || "START".equals(tmpVo.getStatus())  ){
				tmpVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
			}else{
				tmpVo.setChartStatus(CommonCode.MSG_WAIT);
			}
		}
		return listInput;
	}
	
	private List<AdapterVo> removeListStatusDuple (AdapterVo preVo, List<AdapterVo> list){
		List<AdapterVo> rtnList = new ArrayList<AdapterVo>(); 
		int size = list.size();
		if(1 > size){
			return null;
		}
		
		AdapterVo innerPreVo = new AdapterVo();
		innerPreVo = preVo;
		for(int i = 0 ; i < size;i++){
			AdapterVo tmpVo = list.get(i);
			
			//logger.info("tmpVo:"+tmpVo);
// �쓽誘몄뾾�뒗 遺�遺꾧컳�븘 二쇱꽍. //add comment cannon			
			// 議곌굔 1. incycle �븘�땲硫댁꽌 李⑦듃 �긽�깭媛� �삊媛숇떎.
			// 議곌굔 2. incycle �씠硫댁꽌 �씠�쟾�씠�옉 �삊媛숆퀬 spd媛� �젣濡쒓퀬...
			// 議곌굔3. 洹몄쇅�뒗 以묐났 �븘�떂.
//			if(!innerPreVo.getChartStatus().equals(CommonCode.MSG_IN_CYCLE)
//					&&  innerPreVo.getChartStatus().equals(tmpVo.getChartStatus()) ){
//				logger.error("@@@@@CASE1");
//				logger.error("STATUS DUPLE");
//				continue;
//			}else if(innerPreVo.getChartStatus().equals(CommonCode.MSG_IN_CYCLE)
//				&& innerPreVo.getChartStatus().equals(tmpVo.getChartStatus())
//				&& innerPreVo.getIsZeroSpdLoad().equals(tmpVo.getIsZeroSpdLoad())){
//				logger.error("@@@@@CASE2");
//				logger.error("INCYCLE DUPLE and spdLoad is zero.");
//				continue;
//			}else{
//				rtnList.add(tmpVo);
//				innerPreVo = tmpVo;
//				logger.error("@@@@@CASE3");
//				logger.error("NO DUPLE.");
//			}
			//end comment cannon.
			rtnList.add(tmpVo);
			innerPreVo = tmpVo;
		}
		return rtnList;
	}
	
	
	
	public String editDvcHealth(DeviceVo dvcVo)
	{
		logger.info("RUN editDvcHealth");
		SqlSession sql = getSqlSession();
		sql.update(DEVICE_SPACE + "editDvcHealth",dvcVo);
		
		return "OK";
	}
	
	private List<AdapterVo> setListEndTime(List<AdapterVo> listInput){
		int size = listInput.size();
		for(int i=0 ; i<size ; i++){
			AdapterVo tmpVo = listInput.get(i);
			//logger.error("["+i+"]@@@@@setListEndTime:"+tmpVo);
			if(i != size - 1){
				tmpVo.setEndDateTime(listInput.get(i+1).getStartDateTime()); 
				
				//logger.error("["+i+"]i+1 startDateTime:"+listInput.get(i+1).getStartDateTime());

			}
		}
		return listInput;
	}
	
	public String editLastDvcStatus(DeviceVo inputVo)
	{
		logger.error("RUN editLastDvcStatus:"+inputVo);
		SqlSession sqlSession=getSqlSession();
		sqlSession.update(DEVICE_SPACE + "editDvcLastStatus", inputVo);

		return "OK";
	}
	
	public String editLastStatusIOL(DeviceVo inputVo)
	{

		SqlSession sqlSession=getSqlSession();
		sqlSession.update(DEVICE_SPACE + "editDvcLastStatusIOL", inputVo);

		return "OK";
	}

	@Override
	public void getIOLData()
	{
		SqlSession sql = getSqlSession();
		
		List<AdapterVo> listAdt = getListIOL();
		int size = listAdt.size();
		logger.info("number of IOL:"+size);
		AdapterVo tmpVo = new AdapterVo();
		for(int i = 0 ; i<size ;i++){
			try {
				tmpVo = listAdt.get(i);
				
				URL url = new URL(CommonCode.MSG_HTTP + tmpVo.getAdtIp() + CommonCode.MSG_IOL_URL);
				URLConnection con = url.openConnection();
				con.setConnectTimeout(CommonCode.CONNECT_TIMEOUT);
				con.setReadTimeout(CommonCode.READ_TIMEOUT);
				InputStream in = con.getInputStream();
				
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String strTemp = org.apache.commons.io.IOUtils.toString(br);
				AdapterVo pureVo = getIOLStatus(strTemp,tmpVo.getAdtId());

				DeviceVo setVo = new DeviceVo();
				setVo.setAdtId(tmpVo.getAdtId());
				setVo.setLastUpdateTime(pureVo.getStartDateTime());
				setVo.setLastChartStatus(pureVo.getChartStatus());
				setVo.setLastStartDateTime(pureVo.getStartDateTime());
				//setVo.setWorkDate(CommonFunction.getWorkDate(System.currentTimeMillis()));
				
				editLastStatusIOL(setVo);
				
				AdapterVo preIOLVo = getLastInputData(pureVo);
				preIOLVo = chkIOLStatus(preIOLVo);
				
				AdapterVo startVo = chkDateStarterIOL(preIOLVo, pureVo);
				
				//logger.info("Before Duple:"+preIOLVo.getEndDateTime());
				if(null==isDupleIOL(preIOLVo, pureVo)
						&& null == preIOLVo.getEndDateTime()
					){//check Duplication.
					//logger.info("RUN nothing");
				}else{
					if(null != startVo){
						pureVo.setStartDateTime(startVo.getEndDateTime());;
					}
					
					editLastEndTime(pureVo);
					addIOLStatus(pureVo);
				}
				
			} catch (SocketTimeoutException ex) {
				logger.error("[ADT_ID:"+tmpVo.getAdtId()+"@@]"+"IOL NO-CONNECTION:"+"[IP:"+ tmpVo.getAdtIp()+"]");
			}catch (UnknownHostException exx){
				logger.error("[ADT_ID:"+tmpVo.getAdtId()+"@@]"+"UnknownHostException Error");
			} catch (Exception e){
				logger.error("[ADT_ID:"+tmpVo.getAdtId()+"@@]"+"IOL Error");
				e.printStackTrace();
			} 
		}
		
	}
	
	public AdapterVo getIOLStatus(String getResult,String adtId){
		AdapterVo pureVo = new AdapterVo();
		
		String[] array;
		array = getResult.split("=|\\<");
		pureVo.setAdtId(adtId);
		
		if(adtId.equals("23")||adtId.equals("24")||adtId.equals("25")){
			pureVo.setIoPower((null != array[1] && array[1].equals("1")) ? true : false );
		    pureVo.setIoInCycle((null != array[3] && array[3].equals("1")) ? true : false );
		    pureVo.setIoWait((null != array[5] && array[5].equals("1")) ? true : false );
		    pureVo.setIoAlarm((null != array[7] && array[7].equals("1")) ? true : false );
		}else{// origin.
			pureVo.setIoPower((null != array[1] && array[1].equals("1")) ? true : false );
		    pureVo.setIoInCycle((null != array[3] && array[3].equals("1")) ? true : false );
		    pureVo.setIoAlarm((null != array[5] && array[5].equals("1")) ? true : false );
		    pureVo.setIoWait((null != array[7] && array[7].equals("1")) ? true : false );
		}
	    
	    pureVo.setStatus(array[1]+array[3]+array[5]+array[7]);
	    
//	    if(pureVo.getIoPower()==false){
//	    	pureVo.setChartStatus(CommonCode.MSG_NO_CONNECTION);
//	    }else
	    if(pureVo.getIoAlarm()){
	    	pureVo.setChartStatus(CommonCode.MSG_ALARM);
	    }else if (pureVo.getIoWait()){
	    	pureVo.setChartStatus(CommonCode.MSG_WAIT);
	    }else if(pureVo.getIoInCycle()){
	    	pureVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
	    }else{
	    	pureVo.setChartStatus(CommonCode.MSG_WAIT);
	    }
	    
	    long crntMillTime = System.currentTimeMillis();
	    
	    pureVo.setStartDateTime(CommonFunction.unixTime2Datetime(crntMillTime));
	    pureVo.setWorkDate(CommonFunction.mil2WorkDate(crntMillTime));
	    
		return pureVo;
	}
	
	public AdapterVo chkIOLStatus(AdapterVo inputVo){
		
		String adtId = inputVo.getAdtId();
		String ioStatus = inputVo.getStatus();
		if(ioStatus == null || ioStatus.length() != CommonCode.IOL_STATUS_LENGTH){
			return inputVo;
		}
		
		//NHM 8000 #1,2,3
		if(adtId.equals("23")||adtId.equals("24")||adtId.equals("25")){
			inputVo.setIoPower((ioStatus.charAt(0)=='1') ? true : false);
			inputVo.setIoInCycle((ioStatus.charAt(1)=='1') ? true : false);
			inputVo.setIoWait((ioStatus.charAt(2)=='1') ? true : false);
			inputVo.setIoAlarm((ioStatus.charAt(3)=='1') ? true : false);
		}else{
			inputVo.setIoPower((ioStatus.charAt(0)=='1') ? true : false);
			inputVo.setIoInCycle((ioStatus.charAt(1)=='1') ? true : false);
			inputVo.setIoAlarm((ioStatus.charAt(2)=='1') ? true : false);
			inputVo.setIoWait((ioStatus.charAt(3)=='1') ? true : false);
		}
		
	    if(inputVo.getIoAlarm()){
	    	inputVo.setChartStatus(CommonCode.MSG_ALARM);
	    }else if(inputVo.getIoWait()){
	    	inputVo.setChartStatus(CommonCode.MSG_WAIT);
	    }else{
	    	inputVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
	    }
	    
		return inputVo;
	}
	
	public AdapterVo chkDateStarterIOL(AdapterVo preVo, AdapterVo crtVo){

		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			logger.error("@@@@@RUN DateStarter IOL@@@@@");
			logger.error("startDateTime:"+preVo.getStartDateTime());
			logger.error("endDateTime:"+crtVo.getStartDateTime());
			
			AdapterVo starterVo = new AdapterVo();
			starterVo.setAdtId(crtVo.getAdtId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setEndDateTime(CommonFunction.getStandardP1SecToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
			//list.add(i, starterVo);
			editLastEndTime(starterVo);
			addIOLStatus(starterVo);

			return starterVo;
		}
		return null;
	}
	
	public AdapterVo isDupleIOL(AdapterVo preIOLVo, AdapterVo inputVo){
		Long preStartTime = 0L;
		if(null == preIOLVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preIOLVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		if(crtStartTime < preStartTime){
			logger.error("timeTrash data");
			return null;
		}
    	if( preIOLVo.getStatus() != null && preIOLVo.getStatus().equals(inputVo.getStatus())){
    		
    	    	return null;
    	}else{
    		return inputVo;
    	}
	}
	
		
	
		
	public AdapterVo chkDateStarterAGT(AdapterVo preVo, AdapterVo crtVo){
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			logger.error("@@@@@start Date Time AGT@@@@@");
			logger.error("startDateTime:"+preVo.getStartDateTime());
			logger.error("endDateTime:"+crtVo.getStartDateTime());
			AdapterVo starterVo = new AdapterVo();
			starterVo.setAdtId(crtVo.getAdtId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));

			editLastEndTime(starterVo);
			addPureStatus(starterVo);
			
			return starterVo;
		}
		return null;
		
	}
	private AdapterVo setAdtStatus(AdapterVo inputVo){
		logger.info("setAdtStatus_inputVo:"+inputVo);
		if (Float.valueOf(inputVo.getSpdLoad()) == 0 ){
			inputVo.setIsZeroSpdLoad(true);
		}else{
			inputVo.setIsZeroSpdLoad(false);
		}
		
		if (Integer.valueOf(inputVo.getFeedOverride()) == 0 ){
			inputVo.setIsZeroFeedOverride(true);
		}else{
			inputVo.setIsZeroFeedOverride(false);
		}
		
		if (Integer.valueOf(inputVo.getActualFeed()) == 0 ){
			inputVo.setIsZeroActualFeed(true);
		}else{
			inputVo.setIsZeroActualFeed(false);
		}
		
		logger.info("inputVo.getSpdActualSpeed():"+inputVo.getSpdActualSpeed());
		if (Integer.valueOf(inputVo.getSpdActualSpeed()) == 0 ){
			logger.info("case 0 ");
			inputVo.setIsZeroActualSpindle(true);
		}else{
			logger.info("else case");
			inputVo.setIsZeroActualSpindle(false);
		}
		
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonModal;
		JSONArray listAuxModal;
		try {
			String tmpModal = inputVo.getModal();
			//Maybe IOLogic case.
			if(tmpModal== null || tmpModal.length()<1){
				return inputVo;
			}
				
			jsonModal = (JSONObject) jsonParser.parse(tmpModal);
			inputVo.setModal_m1("");
			inputVo.setModal_m2("");
			inputVo.setModal_m3("");
			inputVo.setModal_t("");
			if(jsonModal != null ){
				listAuxModal = (JSONArray) jsonParser.parse(jsonModal.get("AUX_CODE")+"");
				int auxSize = listAuxModal.size();
				
				for(int j = 0 ; j<auxSize ; j++){
					JSONObject tmpAux = (JSONObject)listAuxModal.get(j);
					
					String tmp;
					tmp = (null == tmpAux.get("M1")) ? "" :tmpAux.get("M1")+"";
					inputVo.setModal_m1(tmp);
					tmp = (null == tmpAux.get("M2")) ? "" :tmpAux.get("M2")+"";
					inputVo.setModal_m2(tmp);
					tmp = (null == tmpAux.get("M3")) ? "" :tmpAux.get("M3")+"";
					inputVo.setModal_m3(tmp);
					tmp = (null == tmpAux.get("T")) ? "" :tmpAux.get("T")+"";
					inputVo.setModal_t(tmp);

				}
				
				JSONArray listGModal = (JSONArray) jsonParser.parse(jsonModal.get("G_MODAL")+"");
				int gModalSize = listGModal.size();
				for(int j = 0 ;j<gModalSize;j++){
					JSONObject tmpGModal = (JSONObject)listGModal.get(j);
					if(null != tmpGModal.get("G0")){
						inputVo.setModal_g0(tmpGModal.get("G0")+"");
					}
				}
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			logger.error(e.toString());
		}
		
		return inputVo;
	}
	
	//
	// �떎�떆媛꾩쑝濡� alarm怨� wait �긽�깭瑜� �꽔�뒗�떎.
	// dvcId
	// end媛� null�씤 �긽�깭�뱾�� 5遺� �떒�쐞濡� �뒪耳�伊� �룎硫댁꽌 媛� 梨꾩썙 �꽔�뒗�떎.
	// 留뚯빟 endTime怨� 媛숈� starttime�쓣 媛�吏��뒗 dateStarter媛� �엳�쓣 寃쎌슦�뿉�뒗 洹� �떎�쓬endtime�쓣 �꽔�뒗�떎.(null�씪�닔�룄 �엳�쓬)
	private void addListAWStatus(List<AdapterVo> listPureStatus){
		logger.info("addListAWStatus");
		SqlSession sql=getSqlSession();
		
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		int listSize = listPureStatus.size();
		
		List<AWVo> inputList = new ArrayList<AWVo>();
		
		if(listSize<1){
			return;
		}
		
		for(int i =0;i<listSize;i++){
			AdapterVo tmpVo = listPureStatus.get(i);
			
			
			if(tmpVo.getChartStatus().equals(CommonCode.MSG_ALARM)
				|| tmpVo.getChartStatus().equals(CommonCode.MSG_WAIT)){
				//logger.info("ALARM AND WAIT CASE!");
//				if( i > 0 ||tmpVo.getChartStatus() == listPureStatus.get(i-1).getChartStatus()){
//					continue;
//				}
				AWVo inputVo = new AWVo();
				inputVo.setAdtId(tmpVo.getAdtId());
				inputVo.setChartStatus(tmpVo.getChartStatus());
				inputVo.setWorkDate(tmpVo.getWorkDate());
				inputVo.setStartDateTime(tmpVo.getStartDateTime());
				if(tmpVo.getEndDateTime() != null){
					inputVo.setEndDateTime(tmpVo.getEndDateTime());
				}
				
				if(tmpVo.getAlarm() != null){
					//set Alarm code and alarm msg.
					setJsonAlarm(tmpVo.getAlarm(), inputVo);
				}
				
				inputList.add(inputVo);
			}
		}
		
		int awSize = inputList.size();
		
		if(awSize<1){
			return;
		}
		
		dataMap.put("listAWStatus", inputList);
		dataMap.put("adtId",inputList.get(0).getAdtId());
		sql.delete(ADAPTER_SPACE + "rmvDupleAWData", dataMap);
		sql.insert(ADAPTER_SPACE + "addListAWData", dataMap);
		
	}
	
	private AWVo setJsonAlarm(String inputStrJson, AWVo inputVo){
		if(inputStrJson==null){
			inputVo.setAlarmCode(null);
			inputVo.setAlarmMsg(null);
			return inputVo;
		}
		try {
			 
            JSONParser jsonParser = new JSONParser();
            
            // Alarm 諛곗뿴�쓣 異붿텧.
            // 臾댁“嫄� 泥ル쾲夷� �븣�엺留� 異붿텧.
            JSONArray bookInfoArray = (JSONArray) jsonParser.parse(inputStrJson);
            JSONObject bookObject = (JSONObject) bookInfoArray.get(0);
            inputVo.setAlarmMsg(""+bookObject.get("alarmMsg"));
            inputVo.setAlarmCode(""+bookObject.get("alarmCode"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
		
		return inputVo;
	}
	
	private int getCntAPCPrgm(List<McPrgmVo> inputList, AdapterVo inputVo){
		int rtnInt = 0;
		
		for(McPrgmVo i: inputList){
			//logger.info("i.getPrgmType():"+i.getPrgmType());
			//logger.info("i.getAdtId().intValue():"+i.getAdtId().intValue());
			//logger.info("Integer.parseInt(inputVo.getAdtId():"+Integer.parseInt(inputVo.getAdtId()));
			//logger.info("i.getPrgmName():"+i.getPrgmName());
			//logger.info("inputVo.getCurrentPrgmName():"+inputVo.getCurrentPrgmName());

			
			if("APC".equals(i.getPrgmType()) ){
				//logger.info("case1 true");	
			}
			
			if(( i.getAdtId().intValue() == Integer.parseInt(inputVo.getAdtId()) ) ){
				//logger.info("case2 true");	
			}
			
			if(i.getPrgmName().equals(inputVo.getCurrentPrgmName())){
				//logger.info("case3 true");	
			}
			
			
			
			if("APC".equals(i.getPrgmType())
			    && i.getAdtId().intValue() == (Integer.parseInt(inputVo.getAdtId()))
				&& i.getPrgmName().equals(inputVo.getCurrentPrgmName()) ){
				rtnInt++;
			}
				
		}
		
		return rtnInt;
		
	}
	
}