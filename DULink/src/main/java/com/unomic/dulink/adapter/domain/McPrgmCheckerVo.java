package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.CommonCode;



@Getter
@Setter
@ToString
public class McPrgmCheckerVo extends McPrgmVo{
	
	String startDateTime;
	String endDateTime;
	String dvcName;
	String chartStatus;
	String date;
	String sDate;
	String eDate;
		
}
