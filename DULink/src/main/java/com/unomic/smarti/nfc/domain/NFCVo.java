package com.unomic.smarti.nfc.domain;

import java.util.ArrayList;
import java.util.List;



import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class NFCVo{
	private static int ALARMLIMIT = 2;
	
	String mainProgName;
	String ncStatus;
	String ncMode;
	String date;
	String time;
	String color;
	String feedOverride;
	
	String spindleLoad;
	String alarmNumber1;
	String alarmMsg1;
	String alarmType1;
	String alarmMtesAction1;

	String alarmNumber2;
	String alarmMsg2;
	String alarmType2;
	String alarmMtesAction2;
	String localIp;
	String remoteIp;
	
	String adtId;
	
	List <Alarm> arrAlarm;
	
	String MD5;
	
	Long timeStamp;
	public String getAlarmNumber()
	{
//		String alarmNumber = "";
//		for (int i=0; i < arrAlarm.size(); i++)
//		{
//	        if(arrAlarm.get(i).getAlarmNum().equals(""))
//	        {
//	                // nothing
//	        }else {
//				alarmNumber = arrAlarm.get(i).getAlarmNum();
//				break;
//			}
//		}
		String alarmNumber = String.valueOf(arrAlarm.size());
		return alarmNumber;
	}
	
	public String getAlarmArray()
	{
		String txtAlarm = "";
		for (int i=0; i < arrAlarm.size(); i++)
		{
			if(arrAlarm.get(i).getAlarmNum().equals(""))
			{
				// nothing
			}else{
				txtAlarm += "" + arrAlarm.get(i).getAlarmNum() + " : " +
						arrAlarm.get(i).getAlarmMsg() + ":" + arrAlarm.get(i).getAlarmType() + "\n"; 
			}
		}
		return txtAlarm;
	}
}
