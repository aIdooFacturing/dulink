package com.unomic.smarti.nfc.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Alarm {
	private String alarmNum;
	private String alarmMsg;
	private String alarmType;
	private String alarmMtesAction;
	
public Alarm(String alarmType, String alarmNum,String alarmMsg, String alarmMtesAction){
	this.alarmType = alarmType;
	this.alarmNum = alarmNum;
	this.alarmMsg = alarmMsg;
	this.alarmMtesAction = alarmMtesAction;
}


}
