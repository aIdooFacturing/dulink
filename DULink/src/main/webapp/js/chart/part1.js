var draw;
var background_img;
var marker;
var imgPath = "../images/DashBoard/";
		
var pieChart1;
var pieChart2;
$(function() {
	
	//drawPieChart("pieChart1", "무인장비 가동율", AutoMachine, "%");
	drawPieChart("pieChart2", "현재 가동상태", currentOperating, "대");
	
	//pieChart1 = $("#pieChart1").highcharts();
	pieChart2 = $("#pieChart2").highcharts();
	
	$("#title_main").css({
		"width" : getElSize(500),
		"top" : $("#container").offset().top + (getElSize(25))
	});

	$("#title_main").css({
		"left" : $("#svg").width()/2 - ($("#title_main").width()/2) + sideMargin
	});
	
	$("#title_left").css({
		"width" : getElSize(150),
		"top" : $("#container").offset().top + (getElSize(25))
	});
	
	$("#title_right").css({
		"position" : "absolute",
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(20),
		"top" : $("#container").offset().top + (getElSize(40))
	});

	$("#title_right").css({
		"left" : contentWidth/2 - $("#title_right").width() + sideMargin - getElSize(25)    
	});
	
	$("#title_left").css({
		"left" : getElSize(25) + sideMargin
	});
	
	$("#time").css({
		"top": $("#container").offset().top + getElSize(85),
		"right": $("#svg").offset().left + $("#svg").width() + getElSize(25) ,
		"font-size" : getElSize(15)
	});
	
	$("#date").css({
		"top": $("#container").offset().top + getElSize(85),
		"right": $("#svg").offset().left + $("#svg").width() + getElSize(150),
		"font-size" : getElSize(15)
	});
	
	$("#go123").css({ 
		"width": getElSize(300),
		"top" : $("#container").offset().top + contentHeight/(targetHeight/825),
		"right": contentWidth/2 + sideMargin
	});
	
	$("#Legend").css({
		"font-size" : getElSize(25),
	});
	
	$("#Legend").css({
		"left" : $("#svg").width()/2 - ($("#Legend").width()/2) + sideMargin,
		"top" : $("#svg").height() + $("#container").offset().top - $("#Legend").height() - getElSize(25)
	})
	
	$("#tableDiv").css({
		"left": getElSize(10) + sideMargin,
		"width": getElSize(300), 
		"top": getElSize(225) + $("#container").offset().top
	});
	
	$("#tableHeader").css({
		"font-size" : getElSize(20)
	});
	
	$(".tr1").css({
		"font-size" : 5//getElSize(13)
	});
	
	$(".td1").css({
		"padding" : getElSize(10)
	});
	draw = SVG("svg");
	
	getMachineInfo();
	//getMarker();
	
	background_img = draw.image(imgPath+"Road.png").size(contentWidth * 0.8*0.5, contentHeight*0.5);
//	background_img.x(getElSize(220));
//	background_img.y(contentHeight/(targetHeight/50));
	
	background_img.x(getElSize(650/2));
	background_img.y(getElSize(80/2));
	
	var logoText =  draw.text(function(add) {
		  add.tspan("　　　성역 없는 ").fill("#313131");
		  add.tspan("혁신!").fill("#ff0000");
		  add.tspan("전원이 참여하는 ").fill("#0005D5").newLine();
		  add.tspan("혁신!").fill("#ff0000");
		  add.tspan("　　나로 부터의 ").fill("#c00000").newLine();
		  add.tspan("혁신!").fill("#ff0000");
		  
	});
		
	logoText.font({
			'font-family':'나눔고딕'
			, size:     getElSize(75*0.5)
			, anchor:   'right'
			, leading:  '1.3em'
		   ,'font-weight':'bolder',
	});
	
	logoText.x(getElSize(1280));
	logoText.y(contentHeight/(targetHeight/880));
});

function alarmTimer(){
	setInterval(function(){
		if(s2==10){
    		s++;
    		$("#second1").html(s);
    		s2 = 0;	
    	};
    	
    	if(s==6){
    		s = 0;
    		m2++;
    		
    		$("#second1").html(s);
    		$("#minute2").html(m2);
    	};
    	
    	if(m2==10){
    		m2 = 0;
    		m++;
    		$("#minute1").html(m);
    	};
		
    	$("#second2").html(s2);
    	s2++;
    	
	},1000);
};

function printMachineName(arrayIdx, x, y, w, h, name, color, fontSize){
	machineName[arrayIdx] = draw.text(nl2br(name)).fill(color);
	machineName[arrayIdx].font({
		  family:   'Helvetica'
		, size:    	fontSize/2
		, anchor:   'middle'
		, leading:  '0em'
	});
	
	
	var wMargin = 0;
	var hMargin = 0;
	
	if(name=="SET UP"){
		hMargin = contentHeight/(targetHeight/15);
	}else if(name.indexOf("WASHING")!=-1){
		wMargin = contentHeight/(targetHeight/5);
	}else if(name.indexOf("NHM")!=-1){
		wMargin = -getElSize(5);
	}
	
	machineName[arrayIdx].x(x + (w/2) - wMargin);
	machineName[arrayIdx].y(y + (h/2)-(contentHeight/(targetHeight/20)+hMargin));
	
	machineName[arrayIdx].leading(1);
};

function nl2br(value) {
	  return value.replace(/<br>/g, "\n");
};

function splitStr(str){
	if(str.indexOf("<br>")!=-1){
		var n = str.indexOf("<br>");
		str = str.substr(0,n);
	};
	
	return str;
};
var machineProp = new Array();
var machineList = new Array();
var machineName = new Array();
var machineStatus = new Array();
var newMachineStatus = new Array();
var shopId = 1;
function redrawPieChart(){
	var url = ctxPath + "/svg/getMachineInfo2.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;

			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			$(json).each(function(key, data){
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				};
			});
			
			if(waitMachine==0 && alarmMachine==0){
				borderColor = "blue";
			}else if(alarmMachine==0){
				borderColor = "yellow";
			}else if(alarmMachine!=0){
				borderColor = "red";
			};
			
			bodyNeonEffect(borderColor);
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
		}
	});
};

function replaceAll(str,src, target){
	return str.split(src).join(target);
};

var borderColor = "";
var first = true;
var operationTime = 0;
function getMachineInfo(){
	var url = ctxPath + "/svg/getMachineInfo2.do";
	var param = "shopId=" + shopId;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function (data){
			var json = data.machineList;
			newMachineStatus = new Array();
			
			inCycleMachine = 0;
			waitMachine = 0;
			alarmMachine = 0;
			powerOffMachine = 0;
			
			operationTime = 0;
			$(json).each(function(key, data){
				var array = new Array();
				var machineColor = new Array();
				
				array.push(data.id);
				array.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"));
				array.push(getElSize(data.x));
				array.push(getElSize(data.y));
				array.push(getElSize(data.w*0.5));
				array.push(getElSize(data.h*0.5));
				array.push(data.pic);
				array.push(data.lastChartStatus);
				array.push(getElSize(data.fontSize));
				
				operationTime += Number(data.operationTime);
				
				if(data.lastChartStatus=="IN-CYCLE"){
					inCycleMachine++;
				}else if(data.lastChartStatus=="WAIT"){
					waitMachine++;
				}else if(data.lastChartStatus=="ALARM"){
					alarmMachine++;
				}else if(data.lastChartStatus=="NO-CONNECTION"){
					powerOffMachine++;
				};
				
				machineProp.push(array);
				/*machineProp.push("machine");
				machineProp.push("name");*/
				
				machineColor.push(data.id);
				machineColor.push(data.lastChartStatus);
				
				newMachineStatus.push(machineColor);
			});
			
			pieChart2.series[0].data[0].update(inCycleMachine);
			pieChart2.series[0].data[1].update(waitMachine);
			pieChart2.series[0].data[2].update(alarmMachine);
			pieChart2.series[0].data[3].update(powerOffMachine);
			
			if(!compare(machineStatus,newMachineStatus)){
				reDrawMachine();
				redrawPieChart();
			};
			
			if(first){
				for(var i = 0; i < machineProp.length; i++){
					
					//array = id, name, x, y, w, h, pic, status, fontSize
					
					drawMachine(machineProp[i][0],
							machineProp[i][1],
							machineProp[i][2],
							machineProp[i][3],
							machineProp[i][4],
							machineProp[i][5],
							machineProp[i][6],
							machineProp[i][7],
							i,
							machineProp[i][8]);
				};
				first = false;
			};
		
			//cal totalOperationRatio
//			totalOperationRatio = 0;
//			for(var i = 0; i < machineArray2.length; i++){
//				totalOperationRatio += machineArray2[i][20]; 
//			};
//
//			var totalMachine = 0;
//			totalMachine += (inCycleMachine + waitMachine + alarmMachine + powerOffMachine);
//			var date = new Date();
//			var hour = (date.getHours() + 4)*60;
//			var minute = date.getMinutes();
//			
//			var totalOperationRatio = Number(operationTime/((hour+minute) * totalMachine)*100).toFixed(1);
			
	//			pieChart1.series[0].data[0].update(Number(totalOperationRatio));
	//			pieChart1.series[0].data[1].update(100-Number(totalOperationRatio));

		}
	});
	
	setTimeout(getMachineInfo, 3000);
};


function reDrawMachine(){
	for(var i = 0; i < machineStatus.length; i++){
		if(machineStatus[i][1]!=newMachineStatus[i][1]){
			machineList[i].remove();
			
			//array = id, name, x, y, w, h, pic, status
			
			drawMachine(machineProp[i][0],
					machineProp[i][1],
					machineProp[i][2],
					machineProp[i][3],
					machineProp[i][4],
					machineProp[i][5],
					machineProp[i][6],
					newMachineStatus[i][1],
					i,
					machineProp[i][8]);
		}
	};
	
	machineStatus = newMachineStatus;
};

function compare ( a, b ){
	var type = typeof a, i, j;
	 
	if( type == "object" ){
		if( a === null ){
			return a === b;
		}else if( Array.isArray(a) ){ //배열인 경우
			//기본필터
	      if( !Array.isArray(b) || a.length != b.length ) return false;
	 
	      //요소를 순회하면서 재귀적으로 검증한다.
	      for( i = 0, j = a.length ; i < j ; i++ ){
	        if(!compare(a[i], b[i]))return false;
	      	};
	      return true;
	    };
	  };
	 
	  return a === b;
};

function getMarker(){
	var url = ctxPath + "/svg/getMarker2.do";
	
	$.ajax({
		url : url,
		type: "post",
		dataType : "json",
		success : function(data){
			marker = draw.image(imgPath + data.pic + ".png").size(data.w*0.5, data.h);
			marker.x(data.x);
			marker.y(data.y);
			
			
			marker.draggable();
			marker.dragmove  = function(delta, evt){
				var id = data.id;
				var x = marker.x();
				var y = marker.y();
				
				//DB Access
				setMachinePos(id, x, y);
			};
		}
	});
};

function drawMachine(id, name, x, y, w, h, pic, status, i, fontSize){
	var timeStamp = new Date().getMilliseconds();
	var svgFile;
	if(status==null){
		svgFile = ".svg";
	}else{
		svgFile = "-" + status + ".svg";
	};
	
	machineList[i] = draw.image(imgPath + pic + svgFile + "?dummy=" + timeStamp).size(w, h);
	machineList[i].x(x);
	machineList[i].y(y);
	
	var text_color;
	if(status=="WAIT"  || status=="NO-CONNECTION"){
		text_color = "#000000";
	}else{
		text_color = "#ffffff";
	};
	
	//idx, x, y, w, h, name
	printMachineName(i, x, y, w, h, name, text_color, fontSize);
	
	//idx, machineId, w, h
	setDraggable(i, id, w, h);
	
	//alarm timer
	/*if(status=="ALARM"){
		drawAlarmTimer(x, y, w, h, id);
	};*/
};

function drawAlarmTimer(x, y, w, h, id){
	var timerDiv = $("<div id='timer'>"+
								"<table>"+
								"<tr>"+
									"<tD>"+
										"<div id='minute1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='minute2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD valign='middle'>"+
										"<font  style='font-size: 30px; font-weight:bolder;'>:</font>"+
									"</tD>"+
									"<tD>"+
										"<div id='second1-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
									"<tD>"+
										"<div id='second2-" + id + "' class='odometer'>0</div>"+
									"</tD>"+
								"</tr>"+
							"</table>"+
						"</div>");
	
	$(timerDiv).css({
		"position" : "absolute",
		//"width" : 200,
		"left" : x + (w/2) - (100),
		"top" : y + (h+5)
	});
	
	$("#odometerDiv").append(timerDiv);
};

function setDraggable(arrayIdx, id, w, h){
	machineList[arrayIdx].draggable();
	
	machineList[arrayIdx].dragmove  = function(delta, evt){
		var x = machineList[arrayIdx].x();
		var y = machineList[arrayIdx].y();
		
		//setNamePos
		machineName[arrayIdx].x(x + (w/2) - 30);
		machineName[arrayIdx].y(y + (h/2) - 10);
		
		//DB Access
		setMachinePos(id, x, y);
	};
};

function setMachinePos(id, x, y){
	var url = ctxPath + "/svg/setMachinePos2.do";
	var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){}
	});
};

var inCycleMachine = 1; 
var waitMachine = 1;
var alarmMachine = 1;
var powerOffMachine = 1;

var operationRatio = 1;
var powerOffRatio = 99;
var totalOperationRatio = 0;
var currentOperating = [
                		["가동장비", inCycleMachine],
                		["대기장비", waitMachine],
                	["Alarm 장비", alarmMachine],
                		["기타 전원 Off", powerOffMachine]
                ];
var AutoMachine = [
           			["가동율",operationRatio],
              {name: 'Others',
           			y:powerOffRatio,
           			color : "rgba(0,0,0,0)",
           			dataLabels: {
           				enabled: false
                       	}
                    }
           ];

function drawPieChart(id, title, chartData, unit) {
	
	$("#" + id).css({
		"left" : $("#container").offset().left + getElSize(200),
		"top": $("#container").offset().top + contentHeight/(targetHeight/740),
		"height": contentHeight/(targetHeight/400),
		"width": getElSize(400)
	});
	
	Highcharts.setOptions({
	//green yellow red black
	   colors: ['#148F01', '#C7C402', '#ff0000', '#8C9089']
	});
	
	// Radialize the colors
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
	return {
	    radialGradient: { cx: 0.5, cy: 0.5, r: 0.7 },
	    stops: [
	        [0, Highcharts.Color(color).brighten(0.5).get('rgb')], // darken
	        [1, color]
	        
	    ]
	};
	});
	
	/* // Radialize the colors
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
	return {
	    radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
	    stops: [
	        [0, color],
	        [1, Highcharts.Color(color).brighten(0.4).get('rgb')] // darken
	    ]
	};
	});*/
	
	// Build the chart
	$('#' + id)
		.highcharts(
				{
					chart : {
						plotBackgroundColor : null,
						plotBorderWidth : null,
						plotShadow : false,
						backgroundColor : 'rgba(255, 255, 255, 0)',
						type: 'pie',
			            options3d: {
			                enabled: true,
			                alpha: 45
			            }
					},
					credits : false,
					title : {
						text : title,
						y : getElSize(70),
						style : {
							color : "white",
							fontSize : getElSize(20),
							fontWeight : "bold"
						}
					},
					tooltip : {
						enabled : false
					},
					plotOptions : {
						pie : {
							 innerSize: getElSize(60),
							 depth: contentHeight/(targetHeight/25),
							 size:'80%',
							 cursor : 'pointer',
							 dataLabels : {
								 	enabled : true,
								 	format : '{y}' + unit,
								 	connectorColor: '#000000',
								 	distance : 1,
								 	style : {
								 		 color: 'white',
								 		 textShadow: '0px 1px 2px black',
								 		 fontSize : getElSize(15),
								}
							}
						}
					},
					exporting: false,
					series : [ {
						type : 'pie',
						data : chartData
					} ]
				});
	
}