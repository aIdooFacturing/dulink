<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
var jig = window.localStorage.getItem("jig");
var shopId = 1;
$(function(){
	$("#jigSelector").html(jig)
	var sDate = window.localStorage.getItem("jig_sDate");
	var eDate = window.localStorage.getItem("jig_eDate");
	
	$("#sDate").val(sDate);
	$("#eDate").val(eDate);
	
	$(".date").change(getDvcList);
	
	//setDate();
	getDvcList();
	if(jig==null)$("#jigSelector").html("선택");
	getJigList();
	setEl();
	$("#jigSelector").click(showJigList);
	
	$("#jigList tr").click(function(){
		var jig = $(this).text();
		$("#jigSelector").html(jig)
		clearMenu()
	});
	
	//getMousePos();
	$("#jigSelector").click(showJigList);
	
	$("#jigList tr").click(function(){
		var jig = $(this).text();
		$("#jigSelector").html(jig)
		clearMenu()
	});
	

	$("#menu_btn").click(function(){
		if(!panel){
			showPanel();
		}else{
			closePanel();
		};
		panel = !panel;
	});
	$(".menu").click(goReport);
	
	$("#table").click(function(){
		location.href = "${ctxPath}/chart/performanceReport.do";
	});
})

var wcList = new Array();
var wcName = new Array();

function getDvcList(){
	var url = "${ctxPath}/chart/getTableData.do";

	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val();
	var jig = $("#jigSelector").html();
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&jig=" + jig +
				"&shopId=" + shopId;
	
	window.localStorage.setItem("jig_sDate", sDate);
	window.localStorage.setItem("jig_eDate", eDate);
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.tableData;
			
			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;
			
			var tr = "";

			wcList = new Array();
			var wc = new Array();
			wc.push("구분");
			wc.push("가동시간");
			wc.push("대기");
			wc.push("중단");
			wc.push("전원 Off");
			
			wcList.push(wc);
			
			inCycleBar = new Array();
			waitBar = new Array();
			alarmBar = new Array();
			noConnBar = new Array();
			wcName = new Array();
			$(json).each(function(idx, data){
				wcName.push(data.name);
					 
				var wc = new Array();
				wc.push(data.name);
				wc.push(data.inCycle_time);
				wc.push(data.wait_time);
				wc.push(data.alarm_time);
				wc.push(Number(Number(n * 24 * 60 * 60 - (data.inCycle_time + data.wait_time + data.alarm_time )).toFixed(1)));
				wc.push(data.WC);
				
				wcList.push(wc);
				
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				inCycleBar.push(incycle);
				waitBar.push(wait);
				alarmBar.push(alarm);
				noConnBar.push(noconn);
			});
		
			var blank = maxBar - json.length
			
			for(var i = 0; i < blank; i++){
				wcName.push("");
				var wc = new Array();
				wc.push("________");
				wc.push("");
				wc.push("");
				wc.push("");
				wc.push("");
				
				wcList.push(wc);
				
				inCycleBar.push(0);
				waitBar.push(0);
				alarmBar.push(0);
				noConnBar.push(0);
			};
			
			$(".chartTable").remove();
			var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
			
			for(var i = 0; i < wcList[0].length; i++){
				table += "<tr class='contentTr'>";
				for(var j = 0; j < wcList.length; j++){
					if(j==0){
						table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + getElSize(300) + "'>" + wcList[j][i] + "</td>";
					}else if(j==0 || i==0){
						table += "<td style='font-weight: bolder; background-color: rgb(34,34,34); width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\"," + "\"" + wcList[j][i+5] + "\")'>" + wcList[j][i] + "</td>";
					}else {
						var n;
						if(typeof(wcList[j][i])=="number"){
							n = Number(wcList[j][i]/60/60).toFixed(1)
						}else{
							n = "";
						};
						table += "<td>" + n + "</td>";
					};
				};
				table += "</tr>";
			};
			
			table += "</table>";
			$("#tableContainer").append(table)
	
			setEl();
			chart("chart");
			addSeries();
		//	$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
		}
	});
};

function toDailyChart(dvc, wc){
	var sDate = $("#sDate").val();
	var eDate = $("#eDate").val()
	var url = "${ctxPath}/chart/dailyChart.do?name=" + replaceHash(dvc) +
											"&sDate=" + sDate + 
											"&eDate=" + eDate + 
											"&WC=" + wc;
	
	
	location.href = url;	
};

function replaceHash(str){
	return str.replace(/#/gi,"-");
};

function getJigList(){
	var url = "${ctxPath}/chart/getJigList.do";
	var param = "shopId=" + shopId;
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.jigList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr>" + 
							"<td>" + decodeURIComponent(data.jig) + "</td>" + 
					"</tr>";
			});
			
			$("#jigList").html(tr);
			$("#jigList td").css({
				"border" : "1px solid white"
			});
		
			$("#jigList tr").click(function(){
				var jig = $(this).text();
				$("#jigSelector").html(jig.trim())
				clearMenu();
				getDvcList();
			});
		}
	});
};

var panel = false;
var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20)
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};


function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/DIMM.do";
		location.href = url;
	};
};

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};

function showJigList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
		
	});
	
	$("#jigList").css("display", "inline");	
};

function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
	$("#jigList").css("display","none");
	
};

var barChart;
var maxBar = 15;
function chart(id){
	var margin = (originWidth*0.05)/2;
	$("#" + id).css({
		"margin-left" : $("#tableContainer table td:nth(1)").offset().left - margin- getElSize(80),
		"width" : $("#tableContainer table").width() - $("#tableContainer table td:nth(0)").width()-getElSize(20) + getElSize(80),
		"top" : $(".label").offset().top + $(".label").height() + getElSize(50)
	})
	
	$('#' + id).highcharts({
        chart: {
            type: 'column',
            backgroundColor : 'rgb(50, 50, 50)',
            height :contentHeight * 0.5,
           	marginLeft:getElSize(150),
            marginRight:0
        },
        title: {
            text:false
        },
        xAxis: {
            categories: wcName,
            labels : {
            	style : {
            		"color" : "white",
            		"font-size" : getElSize(30)
            	}
            }
        },
        yAxis: {
            min: 0,
            //max : 20,
            title: {
                text: false
            },
            labels : {
            	style : {
            		"color" : "white",
            		"font-size" : getElSize(30)
            	},
            	enabled : true
            },
            //reversed:true 
        },
        tooltip: {
            enabled : false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
            },
            series:{
            	 pointWidth: getElSize(100)
            }
        },
        credits : false,
        exporting : false,
        legend:{
        	enabled : false
        },
        series: []
    });
	
	barChart = $("#" + id).highcharts();
}

function addSeries(){
	/* barChart.addSeries({
		color : "gray",
		data : noConnBar
	}, true); */
	
	barChart.addSeries({
		color : "red",
		data : alarmBar
	}, true);
	
	barChart.addSeries({
		color : "yellow",
		data : waitBar
	}, true);
	
	barChart.addSeries({
		color : "green",
		data : inCycleBar
	}, true);
	
}

var inCycleBar = new Array();
var waitBar = new Array();
var alarmBar = new Array();
var noConnBar = new Array();

var wcDataList = new Array();

function setEl() {
	$(".container").css({
		"width": originWidth,
		"height" : originHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});
	
	$("#chart").css({
		"margin-bottom" : getElSize(100)
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
		"width" : contentWidth * 0.1
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(100),
		"left" : getElSize(20),
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	
	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 12,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(50)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});

	$("#panel_table td").addClass("unSelected_menu");
	
	$(".chartTable").css({
		"margin-top" : getElSize(60),
		"font-size" : getElSize(50),
		//"padding" : getElSize(20),
		"border-collapse" : "collapse"
	});
	
	$(".chartTable td").css({
		"border" : "solid 1px gray"
	});
	
	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$("#jigSelector, .goGraph, .excel, .label, #table").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(35)
	});
	
	$("#jigList").css({
		"width" : $("#jigSelector").width() + getElSize(30),
		"position" : "absolute",
		"left" : $("#jigSelector").offset().left,
		"top" : $("#jigSelector").offset().top + $("#jigSelector").height() + getElSize(50),
		"z-index" : 9999999,
		"display" : "none",
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$("#jigList").css({
		"width" : getElSize(170),
		"position" : "absolute",
		"background-color": "rgb(34,34,34)",
		"left" : $("#jigSelector").offset().left,
		"top" : $("#jigSelector").offset().top + $("#jigSelector").height() + getElSize(50),
		"z-index" : 9999999,
		"display" : "none",
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
};

function goWcGraph(){
	location.href = "${ctxPath}/chart/wcGraph.do";
};
</script>
</head>

<body oncontextmenu="return false">
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" >
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title">두산 공작기계</div>	
	
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr>
				<td id="menu0" class="menu">샵 레이아웃</td>
			</tr>
			<tr>
				<td id="menu1" class="menu">장비별 가동실적 분석</td>
			</tr>
			<tr>
				<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
			</tr>
			<tr>
				<td id="menu3" class="menu">야간 무인 가동 현황 (DMM)</td>
			</tr>
		</table>
	</div>
	<img src="${ctxPath }/images/menu.png" id="menu_btn" >
	
	<table id="jigList" style="color:white; border-collapse: collapse;" ></table>
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
			<center>
				<table class="mainTable">
					<tr>
						<Td align="center" style="color:white; font-weight: bolder;" class="title" >
								장비별 가동실적 분석
						</Td>
					</tr>
				</table>
				
				<div style="width: 95%" class="label">
					직명 <span style="background-color: white; color:black; font-weight: bolder;" id="jigSelector">선택</span>
					가동기간 <input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate">
					<span style="background-color: white; color:black; font-weight: bolder;" id="table">표</span>
				</div>
				
				
				<div id="chartContainer" style="width: 95%">
					<div id="chart" style="width: 100%">
						
					</div>
					
					<div id="tableContainer" width="100%">
					</div>
				</div>
			</center>
		</div>
	</div>
	
</body>
</html>