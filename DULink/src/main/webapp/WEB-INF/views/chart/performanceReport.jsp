<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<style type="text/css">
	.tableContainer{
		width:95%;
		overflow: auto;
	}
</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;
	var wcData;
	var jigData;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	function csvSend(){
		var id = this.id;
		var sDate, eDate;
		var csvOutput;
		
		if(id=="jigExcel"){
			sDate = $("#jig_sdate").val();
			eDate = $("#jig_edate").val();
			csvOutput = jigData;
		}else{
			sDate = $("#wc_sdate").val();
			eDate = $("#wc_edate").val();
			csvOutput = wcData;
		};
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
var jig = window.localStorage.getItem("jig");
var wc = window.localStorage.getItem("wc");
var shopId = 1;

$(function(){
	var jigSdate = window.localStorage.getItem("jig_sDate");
	var jigEdate = window.localStorage.getItem("jig_eDate");
	$("#jig_sdate").val(jigSdate);
	$("#jig_edate").val(jigEdate);
	
	if(jigSdate=="" || jigEdate==""){
		setDate();			
	};
	
	$(".date").change(getTableData);
	$("#jigSelector").html(jig);
	if(jig==null)$("#jigSelector").html("선택");
	$(".excel").click(csvSend);
	getJigList();
	getWcList(jig);
	setEl();
	setEvt();
	//getMousePos();
	$("#menu_btn").click(function(){
		if(!panel){
			showPanel();
		}else{
			closePanel();
		};
		panel = !panel;
	});
	
	$(".menu").click(goReport);
	
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu1").removeClass("unSelected_menu");
	$("#menu1").addClass("selected_menu");
	
	$(".goGraph").click(goGraphPage);
	
	getTableData("jig");
	
	$("#dlgTable").css({
		"width" : "95%",
		"background-color" : "#505050",
		"border-radius" : getElSize(30)
	});
	
	$("#dlgTable").css({
		"position" : "absolute",
		"z-index" : -1,
		"left" : originWidth/2 - ($("#dlgTable").width()/2),
		"top" : originHeight/2 - ($("#dlgTable").height()/2)
	});
	
	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

});

var jigCsv;
var wcCsv;
var selected_dvc;
function getTableData(el){
	var id = this.id;
	var jigTr = false;
	var wcTr = false;
	if(typeof(id)=="undefined") jigTr = $(el).hasClass("jigTr");

	var sDate;
	var eDate;
	var ty;
	var url = "${ctxPath}/chart/getTableData.do";
	if(id=="jig_sdate" || id=="jig_edate" || jigTr || el=="jig"){
		sDate = $("#jig_sdate").val();
		eDate = $("#jig_edate").val();
		ty = "jig=" + $("#jigSelector").html();
		
		window.localStorage.setItem("jig_sDate", sDate);
		window.localStorage.setItem("jig_eDate", eDate);
	}else{
		sDate = $("#wc_sdate").val();
		eDate = $("#wc_edate").val();
		wc = $("#wcSelector").html();
		
		window.localStorage.setItem("wc_sDate", sDate);
		window.localStorage.setItem("wc_eDate", eDate);
		
		showWcData(selected_dvc, wc, sDate, eDate);
		
		return;
	};
	
	var param = "sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&" +ty +
				"&shopId=" + shopId;
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.tableData;
			
			var start = new Date(sDate);
			var end = new Date(eDate);
			var n = (end - start)/(24 * 3600 * 1000)+1;

			$(".contentTr").remove();
			var tr = "";
			jigData = "장비명, 직,WC,WC 그룹,목표 가동시간,총 가동시간,가동일수,가동,대기,중단,전원 Off,목표대비 가동률LINE";
			$(json).each(function(idx, data){
				var incycle = Number(Number(data.inCycle_time/60/60/n).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60/n).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60/n).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				tr += "<tr class='contentTr' onclick='showWcData(\"" + data.name + "\"," + "\"" + data.WC + "\"," + "\"" + sDate + "\"," +  "\"" + eDate + "\"" + ",\"jig\")'>" +
							"<td>" + data.name + "</td>" +
							"<td>" + $("#jigSelector").html() +
							"<td>" + data.WC + "</td>" + 
							"<td>" + decodeURIComponent(data.GRNM) + "</td>" +
							"<td>" + Number(data.target_time/60/60/n).toFixed(1) + "</td>" + 
							"<td>" + Number(data.inCycle_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + n + "</td>" + 
							"<td>" + incycle + "</td>" +
							"<td>" + wait + "</td>" + 
							"<td>" + alarm + "</td>" +
							"<td>" + noconn + "</td>" + 
							"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
					 "</tr>";
					 
				jigData += data.name + "," + 
						$("#jigSelector").html() + "," + 
						data.WC + "," + 
						decodeURIComponent(data.GRNM) + "," +  
						Number(data.target_time/60/60).toFixed(1) + "," +  
						Number(data.inCycle_time/60/60).toFixed(1) + "," + 
						n + "," + 
						incycle + "," + 
						wait + "," + 
						alarm + "," + 
						noconn + "," + 
						Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
			});
			
			
			$("#jigTable").append(tr);
			setEl();
		}
	});
};


function showWcData(name, wc, sDate, eDate, ty){
	selected_dvc = name;
	var url = "${ctxPath}/chart/getWcData.do";
	var param = "WC=" + wc + 
				"&sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&name=" + name + 
				"&shopId=" + shopId;

	$("#wcSelector").html(wc);
	
	if(ty=="jig"){
		$("#wc_sdate").val($("#jig_sdate").val());
		$("#wc_edate").val($("#jig_edate").val());		
	};
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			$(".contentTr2").remove();
			var tr = "";
			wcData = "직,WC,WC 그룹,목표 가동시간,일자,가동	대기,중단,전원 Off,목표대비 가동률LINE";
			$(json).each(function(idx, data){
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				tr += "<tr class='contentTr2')'>" +  
							"<td>" + $("#jigSelector").html() +
							"<td>" + data.WC + "</td>" + 
							"<td>" + decodeURIComponent(data.GRNM) + "</td>" + 
							"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + data.workDate + "</td>" + 
							"<td>" + incycle + "</td>" +
							"<td>" + wait + "</td>" + 
							"<td>" + alarm + "</td>" +
							"<td>" + noconn + "</td>" + 
							"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
					 "</tr>";
					 
				
				wcData += $("#jigSelector").html() + "," +
						data.WC + "," +
						decodeURIComponent(data.GRNM) + "," +
						Number(data.target_time/60/60).toFixed(1) + "," + 
						data.workDate + "," +
						incycle + "," + 
						wait + "," +
						alarm + "," +
						noconn + "," +
						Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
			});
			
			$("#wcTable").append(tr);
			setEl();
			getDvcList(name);
			
			$("#corver").click(function(){
				clearMenu();
			});
			
			$("#corver").css({
				"z-index" : 9,
				"opacity" : 0.7
			});
			
			$("#dlgTable").css({
				"z-index" : 10
			});
		}
	});
};

function showWcDatabyDvc(dvc, sDate, eDate, ty){
	var url = "${ctxPath}/chart/getWcDataByDvc.do";
	var param = "name=" + dvc + 
				"&sDate=" + sDate + 
				"&eDate=" + eDate + 
				"&shopId=" + shopId;

	//$("#wcSelector").html(wc);
	
	if(ty=="jig"){
		$("#wc_sdate").val($("#jig_sdate").val());
		$("#wc_edate").val($("#jig_edate").val());		
	};
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.wcList;
			
			$(".contentTr2").remove();
			var tr = "";
			wcData = "직,WC,WC 그룹,목표 가동시간,일자,가동	대기,중단,전원 Off,목표대비 가동률LINE";
			$(json).each(function(idx, data){
				var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
				var wait = Number(Number(data.wait_time/60/60).toFixed(1));
				var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
				var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
				
				tr += "<tr class='contentTr2')'>" +  
							"<td>" + $("#jigSelector").html() +
							"<td>" + data.WC + "</td>" + 
							"<td>" + decodeURIComponent(data.GRNM) + "</td>" + 
							"<td>" + Number(data.target_time/60/60).toFixed(1) + "</td>" + 
							"<td>" + data.workDate + "</td>" + 
							"<td>" + incycle.toFixed(1) + "</td>" +
							"<td>" + wait + "</td>" + 
							"<td>" + alarm + "</td>" +
							"<td>" + noconn + "</td>" + 
							"<td>" + Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%</td>" + 
					 "</tr>";
					 
				
				wcData += $("#jigSelector").html() + "," +
						data.WC + "," +
						decodeURIComponent(data.GRNM) + "," +
						Number(data.target_time/60/60).toFixed(1) + "," + 
						data.workDate + "," +
						Number(data.inCycle_time/60/60).toFixed(1) + "," + 
						Number(data.wait_time/60/60).toFixed(1) + "," +
						Number(data.alarm_time/60/6).toFixed(1) + "," +
						Number(data.noConnTime/60/60).toFixed(1) + "," +
						Number((data.inCycle_time/data.target_time) * 100).toFixed(1) + "%LINE";
			});
			
			$("#wcTable").append(tr);
			setEl();
			if(ty!="dvcSelector") getDvcList();
		}
	});
};

function getDvcList(dvc){
	var url = "${ctxPath}/chart/getJigList4Report.do";
	var wc = $("#wcSelector").html();
	var param = "WC=" + wc + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.dvcList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr class='dvcTr'>" + 
							"<td >" + decodeURIComponent(data.name) + "</td>" + 
					"</tr>"
			});
			
			$("#dvcList").html(tr);
			$("#dvcList td").css({
				"border" : "1px solid white"
			});
		
			$("#dvcList tr").click(function(){
				var dvc = $(this).text();
				$("#dvcSelector").html(dvc.trim())
				//clearMenu();
				var sDate = $("#wc_sdate").val();
				var eDate = $("#wc_edate").val();
				showWcDatabyDvc(dvc, sDate, eDate, "dvcSelector");
			});

			if(typeof(name)=="undefined") dvc = $("#dvcList tr:nth(0) td").html()
			$("#dvcSelector").html(dvc);
		}
	});
};

function getJigList(){
	var url = "${ctxPath}/chart/getJigList.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.jigList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr class='jigTr'>" + 
							"<td >" + decodeURIComponent(data.jig) + "</td>" + 
					"</tr>"
			});
			
			$("#jigList").html(tr);
			$("#jigList td").css({
				"border" : "1px solid white"
			});
		
			$("#jigList tr").click(function(){
				var jig = $(this).text();
				$("#jigSelector").html(jig.trim())
				clearMenu();
				window.localStorage.setItem("jig", jig);
				getWcList(jig);
				getTableData(this);
			});
		}
	});
};

function getWcList(jig){
	var url = "${ctxPath}/chart/getWcList.do";
	var param = "jig=" + jig + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(json){
			var json = json.wcList;
			
			var tr = "";
			$(json).each(function(idx, data){
				tr += "<tr class='wcTr'>" + 
							"<td>" + decodeURIComponent(data.WC) + "</td>" + 
					"</tr>"
			});
			
			$("#wcList").html(tr);
			$("#wcList td").css({
				"border" : "1px solid white"
			});
			
			$("#wcList tr").click(function(){
				var wc = $(this).text();
				window.localStorage.setItem("wc", wc);
				$("#wcSelector").html(wc.trim())
				//clearMenu();
				getTableData(this);
				$("#dvcSelector").html("선택")
			});
		}
	});
	
};

function replaceHyphen(str){
	return str.replace(/#/gi,"-");	
};

function goGraphPage(){
	var type = this.id;
	var url;
	if(type=="jig"){
		url = "${ctxPath}/chart/jigGraph.do";
	}else{
		var dvc = replaceHyphen($("#dvcSelector").html());
		var wc = $("#wcSelector").html();
		var sDate = $("#wc_sdate").val();
		var eDate = $("#wc_edate").val();
		url = "${ctxPath}/chart/dailyChart.do?WC=" + wc + "&sDate=" + sDate + "&eDate=" + eDate + "&name=" + dvc;
	};
	location.href = url;
};

var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		closePanel();
		panel = false;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/DIMM.do";
		location.href = url;
	};
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};

function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};

function setEvt(){
	$("#jigSelector").click(showJigList);
	$("#wcSelector").click(showWcList);
	$("#dvcSelector").click(dvcList);
	
	$("#wcList tr").click(function(){
		var wc = $(this).text();
		$("#wcSelector").html(wc.trim())
		clearMenu();
	});
	
};

function showJigList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#jigList").toggle();	
};

function dvcList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#dvcList").toggle();	
};

function showWcList(){
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
	
	$("#wcList").toggle();	
};

function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
	$("#dlgTable").css("z-index",-1);
	$("#wcList").css("display","none");	
	$("#jigList").css("display","none");
	$("#dvcList").css("display","none");
};

function setEl() {
	$(".container").css({
		"width": originWidth,
		"height" : originHeight,
		//"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	
	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(100),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});

	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 12,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});
	
	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(50)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$(".tableContainer").css({
		"height" : getElSize(1600)
	});
	
	$("#jigSelector, .goGraph, .excel, .label, #wcSelector, #dvcSelector").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".tmpTable, .tmpTable tr, .tmpTable td").css({
		//"border": getElSize(5) + "px solid rgb(50,50,50)"
		"border": getElSize(5) + "px solid gray"
	});
	
	$(".tmpTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100)
	});
	
	$(".contentTr, .contentTr2").css({
		"font-size" : getElSize(40)
	});
	
	$("#jigList").css({
		"width" : getElSize(170),
		"position" : "absolute",
		"background-color": "rgb(34,34,34)",
		"left" : $("#jigSelector").offset().left,
		"top" : $("#jigSelector").offset().top + $("#jigSelector").height() + getElSize(50),
		"z-index" : 9999999,
		"display" : "none",
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$("#wcList").css({
		"width" : getElSize(170),
		"position" : "absolute",
		"background-color": "rgb(34,34,34)",
		"left" : $("#wcSelector").offset().left,
		"top" : $("#wcSelector").offset().top + $("#jigSelector").height() + getElSize(50),
		"z-index" : 9999999,
		"display" : "none",
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$("#dvcList").css({
		"width" : getElSize(170),
		"position" : "absolute",
		"background-color": "rgb(34,34,34)",
		"left" : $("#dvcSelector").offset().left,
		"top" : $("#dvcSelector").offset().top + getElSize(80),
		"z-index" : 9999999,
		"display" : "none",
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	}); 
};

</script>
</head>

<body oncontextmenu="return false">
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
		
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" >
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title">두산 공작기계</div>	
	
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr>
				<td id="menu0" class="menu">샵 레이아웃</td>
			</tr>
			<tr>
				<td id="menu1" class="menu">장비별 가동실적 분석</td>
			</tr>
			<tr>
				<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
			</tr>
			<tr>
				<td id="menu3" class="menu">야간 무인 가동 현황 (DMM)</td>
			</tr>
		</table>
	</div>
	<img src="${ctxPath }/images/menu.png" id="menu_btn" >
	
	<table id="jigList" style="color:white; border-collapse: collapse;"></table>
			
	<table id="wcList" style="color:white; border-collapse: collapse;"></table>
	
	<table id="dvcList" style="color:white; border-collapse: collapse;"></table>
			
	<div id="corver"></div>

	<div id="dlgTable">
		<center>
			<div class="label">
				WC명 <span style="background-color: white; color:black; font-weight: bolder;" id="wcSelector">선택</span>
				모델명 <span style="background-color: white; color:black; font-weight: bolder;" id="dvcSelector">선택</span>
				가동기간 <input type="date"	 class="date" id="wc_sdate"> ~ <input type="date" class="date" id="wc_edate">
				<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="goGraph" id="graph">그래프</span>
				<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="excel" id="wcExcel">엑셀</span> 
			</div>
			
			<div class="tableContainer tableContainer2">
				<table style="color: white; width: 100%; text-align: center; border-collapse: collapse; " class="tmpTable" id="wcTable">
					<tr style="font-weight: bolder;background-color: rgb(34,34,34)" class="thead"> 
						<td rowspan="2">직</td>
						<td rowspan="2">WC</td>
						<td rowspan="2">WC 그룹</td>
						<td rowspan="2">목표 가동시간</td>
						<td rowspan="2">일자</td>
						<td colspan="4">일평균</td>
						<td rowspan="2">목표대비 가동률</td>
					</tr>
					<tr style="font-weight: bolder;background-color: rgb(34,34,34)" class="thead">
						<td>가동</td>
						<td>대기</td>
						<td>중단</td>
						<td>전원 Off</td>
					</tr>
				</table>
			</div>
		</center>
	</div>
				
	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
			<center>
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
								장비별 가동실적 분석
						</Td>
					</tr>
				</table>
				
				<div class="label">
					<span class="label" >직명</span> 
					<span style="background-color: white; color:black; font-weight: bolder;" id="jigSelector">선택</span>
					<span class="label">가동기간</span> 
					<input type="date" class="date" id="jig_sdate"> ~ <input type="date" class="date" id="jig_edate">
					<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="goGraph" id="jig">그래프</span>
					<span style="background-color: white; color:black; font-weight: bolder; padding: 3px;" class="excel" id="jigExcel">엑셀</span>
				</div>

				<div class="tableContainer">
					<table style="color: white; width: 100%; text-align: center; border-collapse: collapse; " class="tmpTable" id="jigTable">
						<tr style="font-weight: bolder; background-color: rgb(34,34,34)" class="thead">
							<td rowspan="2">장비명</td> 
							<td rowspan="2">직</td>
							<td rowspan="2">WC</td>
							<td rowspan="2">WC 그룹</td>
							<td rowspan="2">목표 가동시간</td>
							<td rowspan="2">총 가동시간</td>
							<td rowspan="2">가동일수</td>
							<td colspan="4">일평균</td>
							<td rowspan="2">목표대비 가동률</td>
						</tr>
						<tr style="font-weight: bolder; background-color: rgb(34,34,34)" class="thead">
							<td>가동</td>
							<td>대기</td>
							<td>중단</td>
							<td>전원 Off</td>
						</tr>
					</table>
				</div>
			</center>
		</div>
	</div>
</body>
</html>