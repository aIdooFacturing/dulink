<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1">
<title>Dash board Center</title>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
	var pieChart1, pieChart2, pieChart1;
	var barChart1, barChart2, barChart3;

	$(function() {
		setInterval(banner, 5000);
		
		clock();
		setElementPos();
		drawChart("chart1");
		drawChart("chart2");
		drawChart("chart3");

		barChart("bar1", "GT2100");
		barChart("bar2", "Lynx220A");
		barChart("bar3", "NHM6300");

		pieChart1 = $('#chart1').highcharts();
		pieChart2 = $('#chart2').highcharts();
		pieChart3 = $('#chart3').highcharts();

		barChart1 = $('#bar1').highcharts();
		barChart2 = $('#bar2').highcharts();
		barChart3 = $('#bar3').highcharts();

		setAnim();
	});

	function setAnim() {
		setInterval(function() {
			var val1 = Math.floor((Math.random() * 100));
			var val2 = Math.floor((Math.random() * 100));
			var val3 = Math.floor((Math.random() * 100));

			pieChart1.series[0].points[0].update(val1);
			pieChart2.series[0].points[0].update(val2);
			pieChart3.series[0].points[0].update(val3);
		}, 1000)
	};
	
	var flag = true;
	function banner(){
		var duration = 2000;
		var center = $("#center");
		var doosan = $("#doosan");
		
		var $appear;
		var $disappear;
		
		if(flag){
			$appear = doosan;
			$disappear = center;
		}else{
			$appear = center;
			$disappear = doosan;
		};
		
		$disappear.animate({
			bottom : - $disappear.height()
		}, duration, function(){
			$disappear.css("bottom",window.innerHeight);
		});
		
		$appear.animate({
			bottom : 200
		}, duration);
		
		flag = !flag;
	};
	
	
	function drawChart(id) {
		var gaugeOptions = {

			chart : {
				type : 'solidgauge',
				backgroundColor : 'rgba(255, 255, 255, 0)'
			},

			title : null,

			pane : {
				/* center: ['50%', '85%'], */
				size : '90%',
				startAngle : -90,
				endAngle : 90,
				background : {
					backgroundColor : (Highcharts.theme && Highcharts.theme.background2)
							|| '#EEE',
					innerRadius : '60%',
					outerRadius : '100%',
					shape : 'arc'
				}
			},

			tooltip : {
				enabled : false
			},

			// the value axis
			yAxis : {
				stops : [ [ 0.1, '#55BF3B' ], // green
				[ 0.5, '#DDDF0D' ], // yellow
				[ 0.9, '#DF5353' ] // red
				],
				lineWidth : 0,
				minorTickInterval : null,
				tickPixelInterval : 400,
				tickWidth : 0,
				title : {
					y : -70
				},
				labels : {
					y : 16
				}
			},
			
			 exporting: false,
	        
			plotOptions : {
				solidgauge : {
					dataLabels : {
						y : 5,
						borderWidth : 0,
						useHTML : true
					}
				}
			}
		};

		// The speed gauge
		/* $('#' + id)
				.highcharts(
						Highcharts
								.merge(
										gaugeOptions,
										{
											yAxis : {
												min : 0,
												max : 100,
												title : false
											},

											credits : {
												enabled : false
											},

											series : [ {
												name : '',
												data : [ 0 ],
												dataLabels : {
													format : '<div style="text-align:center"><span style="font-size:25px;color:'
															+ ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black')
															+ '">{y}%</span><br/>' /* +
															                       '<span style="font-size:12px;color:silver">km/h</span></div>' */
												},
											} ]

										})); */
	};

	function setElementPos() {
		var width = window.innerWidth;
		var height = window.innerHeight;

		$("tr").css({
			"height" : height * 0.45
		});

		$("#chart1").css({
			"top" : $("#img1").offset().top + $("#img1").height() - 250,
			"left" : $("#img1").offset().left + $("#img1").width() - 300,
		});

		$("#chart2").css({
			"top" : $("#img2").offset().top + $("#img2").height() - 250,
			"left" : $("#img2").offset().left + $("#img2").width() - 300,
		})

		$("#chart3").css({
			"top" : $("#img3").offset().top + $("#img3").height() - 250,
			"left" : $("#img3").offset().left + $("#img3").width() - 300,
		});
		
		$("#center").css({
			"bottom" : 200
		});
		
		$("#doosan").css({
			"bottom" : window.innerHeight,
		});

	};

	function barChart(id, name) {
		$('#' + id).highcharts({
			chart : {
				type : 'bar',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: 200
			},
			credits : false,
			exporting: false,
			title : false,
			xAxis : {
				categories : [ name ],
				labels : {
					style : {
						fontSize : '25px',
						fontWeight:"bold"
					}
				}
			},
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '20px',
				}
			},
			yAxis : {
				min : 1,
				max : 24,
				title : {
					text : false
				},
				labels : {
					style : {
						fontSize : '20px'
					}
				}
			},
			legend : {
				enabled : false
			},
			plotOptions : {
				series : {
					stacking : 'normal',
				}
			},
			series : [ {
				data : [ 15 ],
				shadow : true,
				color : "green"
			},
			{
				data : [ 2 ],
				shadow : true,
				color : "yellow"
			}]
		});
	};

	function clock() {
		// Create two variable with the names of the months and days in an array
		var monthNames = [ "January", "February", "March", "April", "May",
				"June", "July", "August", "September", "October", "November",
				"December" ];
		var dayNames = [ "Sunday", "Monday", "Tuesday", "Wednesday",
				"Thursday", "Friday", "Saturday" ]

		// Create a newDate() object
		var newDate = new Date();
		// Extract the current date from Date object
		newDate.setDate(newDate.getDate());
		// Output the day, date, month and year    
		$('#Date').html(
				dayNames[newDate.getDay()] + " " + newDate.getDate() + ' '
						+ monthNames[newDate.getMonth()] + ' '
						+ newDate.getFullYear());

		setInterval(function() {
			// Create a newDate() object and extract the seconds of the current time on the visitor's
			var seconds = new Date().getSeconds();
			// Add a leading zero to seconds value
			$("#sec").html((seconds < 10 ? "0" : "") + seconds);
		}, 1000);

		setInterval(function() {
			// Create a newDate() object and extract the minutes of the current time on the visitor's
			var minutes = new Date().getMinutes();
			// Add a leading zero to the minutes value
			$("#min").html((minutes < 10 ? "0" : "") + minutes);
		}, 1000);

		setInterval(function() {
			// Create a newDate() object and extract the hours of the current time on the visitor's
			var hours = new Date().getHours();
			// Add a leading zero to the hours value
			$("#hours").html((hours < 10 ? "0" : "") + hours);
		}, 1000);

	}
	
	function nextPage(){
		location.href="${ctxPath}/chart/highChart2.do";
	}
	
	function prePage(){
		location.href="${ctxPath}/chart/highChart3.do";
	}
	
	window.addEventListener("keydown", function (event) {

		  if (event.keyCode == 39) {
			  nextPage();
		  } else if (event.keyCode == 37) {
			  prePage();
		  }
		}, true);
</script>
<style type="text/css">
body {
	overflow: hidden;
	background-color: white;
}

*{
	margin: 0px;
	padding: 0px;
}

img {
	width: 1000px;
}

tr {
	height: 50%;
}

.chart {
	position: absolute;
	height: 600px;
}

.barChart {
	width: 900px;
	height: 300px;
}

#g1 {
	position: absolute;
	top: 220px;
	left: 1430px;
	height: 650px;
}

#g2 {
	position: absolute;
	top: 1000px;
	left: 450px;
	height: 1000px;
}

#g3 {
	position: absolute;
	top: 1000px;
	right: 450px;
	height: 1000px;
}

a {
	text-decoration: none;
	/* color:#00c6ff; */
}

h1 {
	font: 4em normal Arial, Helvetica, sans-serif;
	padding: 20px;
	margin: 0;
	text-align: center;
}

h1 small {
	font: 0.2em normal Arial, Helvetica, sans-serif;
	text-transform: uppercase;
	letter-spacing: 0.2em;
	line-height: 5em;
	display: block;
}

h2 {
	font-weight: 700;
	color: #bbb;
	font-size: 20px;
}

h2,p {
	margin-bottom: 10px;
}

@font-face {
	font-family: 'BebasNeueRegular';
	src: url('BebasNeue-webfont.eot');
	src: url('BebasNeue-webfont.eot?#iefix') format('embedded-opentype'),
		url('BebasNeue-webfont.woff') format('woff'),
		url('BebasNeue-webfont.ttf') format('truetype'),
		url('BebasNeue-webfont.svg#BebasNeueRegular') format('svg');
	font-weight: normal;
	font-style: normal;
}

.container {
	width: 660px;
	margin: 0 auto;
	overflow: hidden;
}

.clock {
	width: 800px;
	margin: 0 auto;
	padding: 30px;
	color: black;
}

#Date {
	font-family: 'BebasNeueRegular', Arial, Helvetica, sans-serif;
	font-size: 30px;
	text-align: center; /* text-shadow:0 0 5px #00c6ff; */
}

ul {
	width: 800px;
	margin: 0 auto;
	padding: 0px;
	list-style: none;
	text-align: center;
}

ul li {
	display: inline;
	font-size: 5em;
	text-align: center;
	font-family: 'BebasNeueRegular', Arial, Helvetica, sans-serif;;
}

#point {
	position: relative;
	-moz-animation: mymove 1s ease infinite;
	-webkit-animation: mymove 1s ease infinite;
	padding-left: 10px;
	padding-right: 10px;
}

@
-webkit-keyframes mymove { 0% {
	opacity: 1.0; /* text-shadow:0 0 20px #00c6ff; */
}

50%
{
opacity


:


0;
text-shadow


:none


;
}
100%
{
opacity


:


1
.0


; /* text-shadow:0 0 20px #00c6ff;  */
}
}
@
-moz-keyframes mymove { 0% {
	opacity: 1.0; /* text-shadow:0 0 20px #00c6ff; */
}

50%
{
opacity


:


0;
text-shadow


:none


;
}
100%
{
opacity


:


1
.0


; /* text-shadow:0 0 20px #00c6ff; */
}
}
.container {
	top : 10px;
	position: absolute;
	right: 150px;
}

#bar {
	position: absolute;
	top: -40px;
	left: -460px;
	height: 2250px;
}

#center, #doosan{
	position: absolute;
	z-index: 10;
	left: 10;
	width: 70px;
}

#machine_name1,#machine_name3{
	width: 230px;
}

#machine_name2{
	width: 400px;
}

.Boxshadow {
   /* -webkit-box-shadow: inset 0 0 50px #000000; */
   position: absolute;
   border-radius : 20px; 
   /* background-color: #FFFAE4; */
   border: 1px solid #2B79FF;
}

#shadow1{
	top: 220px;
	left: 1440px;
}

#shadow2{
	top : 1160px;
	left : 430px;
}

#shadow3{
	top : 1160px;
	right : 430px;
}
#img1, #img2, #img3, #machine_name1, #machine_name2, #machine_name3{
	z-index: 9;
	position: absolute;
}

.barChart, .chart{
	z-index: 10;
}
#img1{
	top : 370px;
	left: 1420px;
}

#img2{
	top : 1350px;
	left : 480px;
}

#img3{
	top : 1350px;
	right : 450px;
}

#machine_name1{
	top: 900px;
	left: 1800px;
}

#machine_name2{
	top: 1850px;
	left: 780px;
}

#machine_name3{
	top: 1850px;
	right: 800px;
}
.logo2{
	width: 3%;
	margin-top: 20px;
	margin-left: 200px;
	margin-bottom: 20px;
}

.light{
	position: absolute;
	width: 50px;
	z-index: 999;
}

#light1{
	top : 720px;
	left: 1810px;
}

#light2{
	top :1550px;
	left: 1160px;
}

#light3{
	top : 1730px;
	left: 2490px;
}
</style>
</head>
<body>
	<%-- <img src="${ctxPath }/images/DashBoard/Green_Light.png" id="light1" class="light">
	<img src="${ctxPath }/images/DashBoard/Green_Light.png" id="light2" class="light">
	<img src="${ctxPath }/images/DashBoard/Green_Light.png" id="light3" class="light"> --%>
	
	<%-- <img src="${ctxPath }/images/DashBoard/logo2.png" class="logo2"> --%> 

<!-- 	<div class ="boxShadow" id="shadow1" style="width: 950px; height: 800px;"></div>
	<div class ="boxShadow" id="shadow2" style="width: 1050px; height: 800px;"></div>
	<div class ="boxShadow" id="shadow3" style="width: 1050px; height: 800px;"></div>
 -->
	<%-- <img src="${ctxPath }/images/machine/center.svg" id="center" >
	<img src="${ctxPath }/images/machine/doosan.svg" id="doosan"> --%>
	<img src="${ctxPath }/images/machine/MetalBar.svg" id="bar" onclick="prePage();">
	<div class="container" onclick="nextPage()">
		<div class="clock">
			<div id="Date"></div>

			<ul>
				<li id="hours"></li>
				<li id="point">:</li>
				<li id="min"></li>
				<li id="point">:</li>
				<li id="sec"></li>
			</ul>

		</div>
	</div>

<%-- 	<img src="${ctxPath }/images/machine/Group2.svg" id="g1">
	<img src="${ctxPath }/images/machine/Group1.svg" id="g2">
	<img src="${ctxPath }/images/machine/Group1.svg" id="g3"> --%>
	<br><br><br>
	<table style="width: 100%">
		<Tr align="center" valign="top">
			<Td colspan="2">
				<div id="bar1" class="barChart"></div> 
				<img src="${ctxPath }/images/pad/GT2100.jpg" id="img1"><br>
				<%-- <img src="${ctxPath }/images/machine/1_g.png" id="machine_name1" class="machine_name"> --%>
				<div id="chart1" class="chart"></div>
			</Td>
		</Tr>
		<tr align="center" valign="top">
			<td>
				<div id="bar2" class="barChart"></div> 
				<img src="${ctxPath }/images/pad/Lynx220A.jpg" id="img2"><br>
				<%-- <img src="${ctxPath }/images/machine/2_g.png" class="machine_name" id="machine_name2"> --%>
				<div id="chart2" class="chart"></div>
			</td>
			<td>
				<div id="bar3" class="barChart"></div> 
				<img src="${ctxPath }/images/pad/NHM6300.jpg" id="img3"><br>
				<%-- <img src="${ctxPath }/images/machine/3_g.png" class="machine_name" id="machine_name3"> --%>
				<div id="chart3" class="chart"></div>
			</td>
		</tr>
	</table>

</body>
</html>